<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,

        /*
         * =================== Backend ===================
         */
        \App\Http\Backend\Middleware\EncryptCookies::class,
        \App\Http\Backend\Middleware\VerifyCsrfToken::class
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        /*
         * =================== Frontend ===================
         */
        'auth'          => \App\Http\Frontend\Middleware\Authenticate::class,
        'auth.basic'    => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest'         => \App\Http\Frontend\Middleware\RedirectIfAuthenticated::class,
        /*
         * =================== Backend ===================
         */
        'Auth.Backend'              => \App\Http\Backend\Middleware\Authenticate::class,
        'Auth.Backend.Role'         => \App\Http\Backend\Middleware\RoleMiddleware::class,
    ];
}
