<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes Backend
|--------------------------------------------------------------------------|
|
*/
require(__DIR__.'/Backend/Routes.php');

/*
|--------------------------------------------------------------------------
| Application Routes Frontend
|--------------------------------------------------------------------------|
*/

require(__DIR__.'/Frontend/Routes.php');
