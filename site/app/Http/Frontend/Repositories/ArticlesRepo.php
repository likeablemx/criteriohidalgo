<?php namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Article;
use App\Http\Entities\Category;
use Faker\Provider\tr_TR\DateTime;
use Illuminate\Support\Facades\Cache;

class ArticlesRepo extends BaseRepo
{
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Article;
    }

    /*
     * ============== Obtener datos de artículo ==============
     */
    public  function getArticle($slug, $categoryId = null)
    {
        $article = $this->getModel()->where('active', true);

        !is_null($categoryId)
            ? $article->where('category_id', $categoryId)->where('principal', true)
            : $article->where('slug', $slug);

        $article->withGallery()
            ->whereHasCategory()
            ->withCategoryModules()
            ->with('authors', 'ids', 'tags')
            ->orderBy('date_publishing', 'desc');

        return Cache::remember(sprintf('article_%s_%s', $slug, $categoryId), 5, function () use ($article) {
            return $article->first();
        });
    }

    /*
     * ============== Obtener lista de artículos por categoría ==============
     */
    public function getArticlesForCategory($categoryId, $take, $limit = null)
    {
        $principal  = $this->getArticle(null, $categoryId);

        $articles = $this->model
            ->withGallery()
            ->with(['category' => function($category){
                $category->with('parent');
            }, 'authors', 'ids', 'tags'])
            ->where('active', true)
            ->orderBy('date_publishing', 'desc')
            ->where('date_publishing','<=',  date('Y/m/d'))
            ->skip($limit)
            ->take($take);

        #-------------- Categoría --------------
        $this->whereCategory($articles, $categoryId);

        #-------------- Omitir nota principal --------------
        !empty($principal) ? $articles->whereNotIn('id', [$principal->id]) : null;

        return Cache::remember(sprintf('articlesForCategories_%s_%s_%s', $categoryId, $take, $limit), 5, function () use ($articles) {
            return $articles->get();
        });
    }

    /*
     * ================== Categorías ==================
     */
    private function whereCategory($articles, $categoryId)
    {
        $categories = array();
        $category   = Category::find($categoryId);

        foreach($category->getDescendantsAndSelf() as $item){
            $cat   = Category::find($item->id);
            if($cat->active == 1)
                $categories[] = $item->id;
        }

        #-------------- Categoría raíz --------------
        return $category->isRoot()
            ? $articles->whereIn('category_id', $categories)
            : $articles->where('category_id', $categoryId)
        ;
    }

    /*
     * ================== Obtener Artículos Relacionados ==================
     */
    public function getArticlesRelated($article, $related = null)
    {
        $listId = array();

        foreach($article->$related as $item) $listId[] = $related == 'ids' ? $item->article_id : $item->id;

        return Cache::remember(sprintf('articlesForCategories_%s_%s', $article, $related), 5, function () use ($article, $listId, $related) {
            return $this->model
                ->withGallery()
                ->whereHasRelated($article->slug, $listId, $related, $article->id)
                ->where('date_publishing', '<=', date('Y/m/d'))
                ->orderBy('date_publishing', 'desc')
                ->limit(4)
                ->get();
        });
    }

    /*
     * ================== Obtener Artículos más vistos ==================
     */
    public function getArticlesMostViewed()
    {
        return $this->model
            ->withGallery()
            ->where('active', true)
            ->where('date_publishing', '=', new \DateTime('today'))
            ->orderBy('views', 'desc')
            ->limit(4)
            ->get();
    }
    
    public function getCounts($categoryId)
    {
        $articles = $this->model->where('active', true);

        #-------------- Categoría --------------
        $this->whereCategory($articles, $categoryId);

        return $articles->count();
    }
    
    /*
     * BUSCADOR DE NOTAS
     * @param $request
     * @return Resultados que coinciden con titulo e introduccion
     */
    public function search($request)
    {
  
        $find = $request->get('search');
    /// buscamos por titulo
        $queryTitle = $this->getQuery('title',$find);
    /// buscamos por titulo, solo id
        $queryTitleIds = $this->getQuery('title',$find);

    /// obtenemos los id de los resultados
        $resultTitle = collect($queryTitleIds->select('id')->get()->toArray());
    /// unimos los ID's en un mismo array
        $resultTitle->pluck('id')->toArray();
    
    /// buscamos por la introduccion
        $queryContent = $this->getQuery('intro',$find);

        $isToday = false;


        if (preg_match('/[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}/', $find)) {
            $date = \DateTime::createFromFormat('Y/m/d', $find)->setTimezone(new \DateTimeZone('America/Mexico_City'));
            if ($date->setTime(0,0,0) == new \DateTime('today')) {
                $isToday = true;
            }
            $queryContent = $this->getModel()
                ->where('created_at', '>=', $date->setTime(0,0,0)->format('Y-m-d H:i:s'))
                ->where('created_at', '<=', $date->setTime(23,59,59)->format('Y-m-d H:i:s'));
        }

        $timeExpire = $isToday?5:360000;
    
    /// unimos los resultados
        $result = Cache::remember(sprintf('articles_find_%s', $find), $timeExpire, function () use ($queryTitle, $queryContent) {
            $result = collect($queryTitle->where('date_expire', '>=', date('Y/m/d'))->orderBy('created_at', 'desc')->get());
            $result = $result->merge($queryContent->where('date_expire', '>=', date('Y/m/d'))->orderBy('created_at', 'desc')->get());

            return $result;
        });
        
        return $result;
    }
    
    /*
     * CONSULTA PARA BUSQUEDA
     * @param $campo, $palabra clave
     * @return query
     */
    private function getQuery($campo,$find)
    {
        return $this->getModel()
            ->whereHas('category', function($category){
                $category->where('active', true);
            })
            ->where('active',1)
            ->where('date_publishing','<=',  date('Y/m/d'))
            ->where($campo,'LIKE',"%{$find}%");
    }
}
