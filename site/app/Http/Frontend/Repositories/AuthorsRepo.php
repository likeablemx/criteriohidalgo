<?php

namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Author;

class AuthorsRepo extends BaseRepo
{
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Author;
    }

    /*
     * AUTOR
     * @param $slug
     * @return Autor con notas publicadas
     */
    public  function getAuthor($slug,$skip)
    {
        $author = $this->getModel()
             ->where('slug',$slug)//identificamos al autor por su slug
             ->with([
                   'articles' => function($article) use ($skip){ 
                 $article->where('active',1)         // solo las notas que estan activas
                         ->where('date_publishing','<=',  date('Y/m/d')) /// validar fecha de publicacion y despublicacion
                         ->orderBy('date_publishing','desc') // ordenamos de la mas reciente a la mas viejita
                         ->with([
                             'galleries' => function($gallery){
                             $gallery->with('elements');
                             }],'category')
                         ->skip($skip)
                         ->take(9);
             }])   
             ->first();

        if(count($author->articles) == 0) abort(404);     
        
        return $author;
    }
    
    public function getCounts($slug)
    {
        $author = $this->getModel()
             ->where('slug',$slug)//identificamos al autor por su slug
             ->with([
                   'articles' => function($article){ 
                 $article->where('active',1)         // solo las notas que estan activas
                         ->where('date_publishing','<=',  date('Y/m/d')); /// validar fecha de publicacion y despublicacion

             }])   
             ->first();
        return count($author->articles);
    }
}
