<?php

namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Directory;

class DirectoryRepo extends BaseRepo
{
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Directory;
    }

    /*
     * DIRECTORIO
     * @return Miembros
     */
    public  function getMembers()
    {
        $dir = $this->getModel()
                ->all()
                ->toArray();
        return array_chunk($dir, 8);      // lo dividimos en 2
    }
}
