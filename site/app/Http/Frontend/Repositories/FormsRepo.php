<?php
namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Form;
use App\Http\Entities\Module;
use App\Http\Entities\Response;
use App\Http\Backend\Helpers\ContactFormMailer;
use App\Http\Backend\Helpers\ContactFormValidator;

class FormsRepo {
    protected $model;

    function __construct() {
        $this->model = new Form();
    }
    
    public function getForm()
    {
        $form = $this->model
                    ->where('id',1)
                    ->with('inputs')
                    ->first();
        
        if($form) {
            foreach($form->inputs as &$input) {
                $input->optionsArray = json_decode($input->options);
            }
            unset($input);
        }
        
        return $form ? $form : abort(404);
    }

    public function saveMessage() {
        //$module = Module::find($moduleId);
        $form = Form::where('id',1)->with('inputs')->first();

        if(ContactFormValidator::passes($form->inputs,\Request::all())) {
            $values = ContactFormValidator::getData($form->inputs,\Request::all());

            $values = array_except($values,'g-recaptcha-response');

            $data = [
                'form_id' => $form->id,
                'values'  => json_encode($values),
            ];
            // Guardamos la respuesta
            Response::create($data);

            // mandamos la respuesta al usuario
            ContactFormMailer::autoResponder($form,$values);

            // mandamos la notificacion a los administradores
            ContactFormMailer::notify($form,$values);

            $to = redirect()->to($form->thanks);

        } else {
            session()->flash('forms.errors',ContactFormValidator::getErrors());
            $to = redirect()->back()->withInput();
        }
        return $to;
    }
}