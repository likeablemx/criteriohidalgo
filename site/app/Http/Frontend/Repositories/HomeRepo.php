<?php

namespace App\Http\Frontend\Repositories;

use App\Http\Entities\Category;
use App\Http\Entities\Gallery;
use App\Http\Entities\Article;
use App\Http\Entities\Module;
use App\Http\Frontend\Traits\timeFormatsTrait;

class HomeRepo extends BaseRepo
{
    use timeFormatsTrait;   // formato de fecha

    // variables para saber que notas no repetir
    public $noRepit = [];       /// Noticias
    public $noRepitCats = [];   /// Categorias
    /*
     * Instanciar modelo
     */
    public function getModel()
    {
        return new Gallery;
    }
    /*
     * Obtenemos los modulos estaticos (Izquierda) y Slider
     *
     * @return modulos
     */
    public function getModules()
    {
        $modules[] = $this->getSlider();     /// Slider
        $modules[] = $this->getFeaturing();  /// Destacados

        $mod = Module::select('category_id','params')->where('type_slug','blockLeft')->get(); /// obtenemos los modulos de lado Izquierdp
        $mod_est = [];

        foreach($mod as $item){
            // id de la categoria, numero de elementos
            $mod_est[] = $this->getRecent($item->category_id,$item->params);
            $this->noRepitCats[] = $item->category_id;
        }
        $modules[] = $mod_est;
        $modules[] = $this->getMinute();    /// Minuto a minuto (WebSocket)
        $modules[] = $this->getMinuteCount();    /// Minuto a minuto (WebSocket)
        return $modules;
    }

    /*
     * SLIDER
     * @return slider con elementos
     */
    public  function getSlider()
    {
        $slider = $this->getModel()
            ->where('name','MainSlider')
            ->with('elements')
            ->orderBy('position','asc')
            ->get()
            ->toArray();
        if(empty($slider))
            return '';

        foreach($slider[0]['elements'] as $item)
            $this->noRepit[] = $item['position'];


        return $slider;
    }

    /*
     * Noticias destacadas
     */
    public function getFeaturing()
    {
        $notes = Article::where('featuring',TRUE)
            ->whereNotIn('id',$this->noRepit)               // que no se repitan
            ->where('date_publishing','<=',  date('Y/m/d'))
            ->where('active',1)
            ->with(['galleries' => function($elements){
                $elements->with(['elements' => function($items){
                    $items->orderBy('position','asc');
                }]);
            },'category' => function($parent){
                $parent->with('parent');
            }])
            ->orderBy('date_publishing','desc')
            ->orderBy('created_at','desc')
            ->take(12);
        $notesValidity = $notes->where('date_expire','>=',date('Y/m/d'))->get();
        $array = [];
        //dd($notes);
        if(empty($notesValidity))
            return '';

        /// Ordenamos los datos a mostrar en array
        foreach($notesValidity as $item){
            $this->noRepit[] = $item->id;
            if(empty($item->galleries[0])){
                $img['value'] = '';
                $dir = '';
            }
            else{
                $img = $item->galleries[0]->elements->first();
                $dir = $item->galleries[0]->directory;
            }
            if(is_null($item['category']['parent'])){
                $sub = '';
                $category = $item['category']['slug'];
            }
            else{
                $sub = $item['category']['slug'];
                $category = $item['category']['parent']['slug'];
            }

            $array [] = ['id'       =>  $item['id'],
                'slug'      =>  $item['slug'],
                'title'     =>  $item['title'],
                'intro'     =>  $item['intro'],
                'icon'      =>  $item['category']['image'],
                'category'  =>  $category,
                'subcategory'=>  $sub,
                'date'      =>  $this->dateFormat($item['date_publishing']),
                'dir'       =>  $dir,
                'img'       =>  $img['subtitle'] ? $img['subtitle'] : $img['value'],
                'imgFirst'  =>  $img['value']
            ];
        }
        //dd($array);
        if(empty($array))
            return null;

        return array_chunk($array, 3);      // lo dividimos en 3
    }

    /*
     * Noticias recientes
     * @param   categoria, elementos a mostrar
     * @return  elementos ordenados segun el modulo
     */
    public  function getRecent($category,$num)
    {
        $recent = Article::where('category_id',$category)/// indicamos la categoria
        ->whereNotIn('id',$this->noRepit)
            ->where('date_publishing','<=',  date('Y/m/d'))
            ->where('active',1)
            ->with(['galleries' => function($elements){
                $elements->with(['elements' => function($items){
                    $items->orderBy('position','asc');
                }]);
            },'category' => function($parent){
                $parent->with('parent');
            }])
            ->orderBy('date_publishing','desc')
            ->orderBy('created_at','desc')
            ->take($num);

        $recentValidity = $recent->where('date_expire','>=',date('Y/m/d'))->get();
        if(empty($recentValidity))
            return null;
        /// Ordenamos los datos a mostrar en array
        $array= '';
        foreach($recentValidity as $item){
            $this->noRepit[] = $item['position'];

            if(count($item->galleries) > 0){
                $img = $item->galleries[0]->elements->first();
                $dir = $item->galleries[0]->directory;
            }
            else{
                $img['value'] = '';
                $dir = '';
            }

            if(is_null($item['category']['parent'])){
                $sub = '';
                $category = $item['category']['slug'];
            }
            else{
                $sub = $item['category']['slug'];
                $category = $item['category']['parent']['slug'];
            }
            $array [] = ['id'       =>  $item['id'],
                'slug'      =>  $item['slug'],
                'title'     =>  $item['title'],
                'icon'      =>  $item['category']['image'],
                'category'  =>  $category,
                'categoryT' =>  $item['category']['title'],
                'subcategory'=>  $sub,
                'intro'     =>  $item['intro'],
                'date'      =>  $this->dateFormat($item['date_publishing']),
                'dir'       =>  $dir,
                'img'       =>  $img['subtitle'] ? $img['subtitle'] : $img['value'],
                'imgFirst'  =>  $img['value']
            ];
        }
        //dd($num);
        if(!empty($array)){
            if($num == 12 ){    /// si es ticket
                $order = array_chunk($array, 2);
                return array_chunk($order, 2);
            }
            elseif($num == 15){ /// si es Deportes
                return array_chunk($array, 5);
            }
            else
                return $array;
        }
        else
            return $array;
    }

    /*
     * Minuto a minuto (WebSocket)
     *
     * @return  elementos ordenados a mostrar
     */
    public function getMinute()
    {
        $article = Article::where('date_publishing','<=',  date('Y/m/d'))
            ->whereNotIn('id',$this->noRepit)
            ->whereNotIn('category_id',$this->noRepitCats)
            ->where('featuring',0)
            ->where('active',1)
            ->orderBy('date_publishing','desc')
            ->orderBy('created_at','desc')
            ->take(6);

        $notesValidity = $article->where('date_expire','>=',date('Y/m/d'))->get();
        if(empty($notesValidity))
            return null;
        $array = [];
        foreach ($notesValidity as $item)
        {
            if(is_null($item->category->parent)){
                $link = $item->category->slug.'/'.$item->slug;
            }
            else
                $link = $item->category->parent->slug.'/'.$item->category->slug.'/'.$item->slug;
            $array[] = [
                'dateP'     => (new \DateTime($item->created_at))->getTimestamp(),
                'title'     => $item->title,
                'category'  => $item->category->title,
                'date'      => $this->dateFormat($item->created_at),
                'time'      => $this->hourFormat($item->created_at),
                'link'      => $link,
            ];
        }
        return $array;
    }

    /*
     * Minuto a minuto (WebSocket)
     *
     * @return  elementos ordenados a mostrar
     */
    public function getMoreMinute($lowerThan)
    {
        $article = Article::where('date_publishing','<=',  date('Y/m/d'))
            ->whereNotIn('id',$this->noRepit)
            ->whereNotIn('category_id',$this->noRepitCats)
            ->where('featuring',0)
            ->where('active',1)
            ->where('created_at', '<',  (new \DateTime())->setTimestamp($lowerThan)->format('Y-m-d H:i:s'))
            ->orderBy('date_publishing','desc')
            ->orderBy('created_at','desc')
            ->take(6);

        $notesValidity = $article->where('date_expire','>=',date('Y/m/d'))->get();
        if(empty($notesValidity))
            return null;
        $array = [];
        foreach ($notesValidity as $item)
        {
            if(is_null($item->category->parent)){
                $link = $item->category->slug.'/'.$item->slug;
            }
            else
                $link = $item->category->parent->slug.'/'.$item->category->slug.'/'.$item->slug;
            $array[] = [
                'dateP'     => (new \DateTime($item->created_at))->getTimestamp(),
                'title'     => $item->title,
                'category' => $item->category->title,
                'date'      => $this->dateFormat($item->created_at),
                'time'      => $this->hourFormat($item->created_at),
                'link'      => $link,
            ];
        }
        return $array;
    }

    /*
     * Minuto a minuto (WebSocket)
     *
     * @return  elementos ordenados a mostrar
     */
    public function getMinuteCount()
    {
        $article = Article::where('date_publishing','<=',  date('Y/m/d'))
            ->whereNotIn('id',$this->noRepit)
            ->whereNotIn('category_id',$this->noRepitCats)
            ->where('featuring',0)
            ->where('active',1)
            ->orderBy('date_publishing','desc')
            ->orderBy('created_at','desc')
            ->take(6);

        $minuteCount = $article->where('date_expire','>=',date('Y/m/d'))->count();

        return $minuteCount;
    }

    /*
     * Modulos del lado derecho
     *
     * @return  elementos ordenados a mostrar
     */
    public function getModulesR()
    {
        $mod = Category::where('slug','home')
            ->with(['modules' => function($modules){
                $modules->whereNotIn('type_slug', ['blockLeft'])
                    ->where('visible', true)
                    ->orderBy('position', 'asc');
            }])
            ->first();

        return $mod;
    }
}
