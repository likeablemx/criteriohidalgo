<?php namespace App\Http\Frontend\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Http\Frontend\Requests;
use App\Http\Frontend\Repositories\HomeRepo;
use App\Http\Frontend\Repositories\StaticsRepo;
use App\Http\Frontend\Repositories\SurveysRepo;
use App\Http\Frontend\Repositories\DirectoryRepo;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /*
     * HOME
     * @return vista
     */
    public function index(SurveysRepo $surveyRepo, HomeRepo $homeRepo)
    {
        //\DB::EnableQuerylog();


        $modules = Cache::remember('modules', 5, function () use ($homeRepo){
            return $homeRepo->getModules();
        });

        $modulR = Cache::remember('modulR', 5, function () use ($homeRepo){
            return $homeRepo->getModulesR();
        });
	
	    $data = [
            'title'         =>  'Home',
            'slider'        =>  $modules[0],
            'featuring'     =>  $modules[1],
            'recent'        =>  $modules[2],
            'minute'        =>  $modules[3],
            'minuteCount'   =>  $modules[4],
            'modulesR'      =>  $modulR,
            'voted'         => session('frontend.survey.voted'), // return TRUE si ya voto
        ];
        session()->put('frontend.authorsLimit',''); // session de autores
        //dd(\DB::getquerylog());
	    return view('Frontend.Home.index',$data);
    }
    
    /*
     * VOTAR ENCUESTA
     * @return Estadisticas de la encuesta
     */
    public function surveyVotar(SurveysRepo $surveyRepo,Request $request)
    {
        $data = [
            'survey'   =>  $surveyRepo->setVoto($request->get('id')) // asignamos voto
        ];
        session()->put('frontend.authorsLimit',''); // session de autores
	return view('Frontend.Survey.results',$data);
    }
    
    /*
     * DIRECTORIO
     * @return vista
     */
    public function directory(DirectoryRepo $repo)
    {
        $data = [
            'title'     => 'Directorio',
            'directory' => $repo->getMembers() // obtenemos a los miembros del directorio
        ];
        session()->put('frontend.authorsLimit',''); // session de autores
	return view('Frontend.Statics.directory',$data);
    }
    
    /*
     * NOSOTROS Y AVISO DE PRIVACIDAD
     * @return vista
     */
    public function statics(StaticsRepo $repo, Route $route)
    {
        $section = $repo->getSections($route->getPath()); // obtenemos la ruta
        session()->put('frontend.authorsLimit',''); // session de autores
        return view("Frontend.Statics.{$section->slug}", [
            'section'       => $section,
            'title'         => $section->name,
        ]);
    }
}
