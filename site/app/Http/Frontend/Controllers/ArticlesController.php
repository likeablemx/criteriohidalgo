<?php namespace App\Http\Frontend\Controllers;

use App\Http\Frontend\Repositories\ArticlesRepo;
use App\Http\Frontend\Repositories\CategoryRepo;
use App\Http\Frontend\Repositories\VisitRepo;
use App\Http\Frontend\Repositories\HomeRepo;
use App\Http\Frontend\Requests\SearchRequest;

class ArticlesController extends Controller
{
    protected $category;
    protected $article;
    protected $visit;

    function __construct(ArticlesRepo $article, CategoryRepo $category, VisitRepo $vist)
    {
        parent::__construct();

        $this->article  = $article;
        $this->category = $category;
        $this->visit    = $vist;
    }

    /*
     * ================== Listado de artículos ==================
     */
    public function index($category, $subcategory = null, $article = null)
    {
        $this->notFoundUnless($url = ['category' => null] + $this->getArticleCategory($category));

        !is_null($subcategory)  ? $url = $this->getArticleCategory($subcategory) : null;
        !is_null($article)      ? $url = $this->getArticleCategory($article) : null;


        switch(key($url))
        {
            case 'category':
            case 'subcategory'  :
                return $this->getArticleList($url['subcategory'], $url['id']);
                break;
            case 'article':
            default:
                return $this->getArticle($url['article']);
        }

    }

    /*
     * ================== Lista de artículos por categoría ==================
     */
    public function getArticleList($list, $categoryId)
    {
        return view('Frontend.Article.list', [
            'category'  => $this->notFoundUnless($category = $this->category->getCategory($categoryId)),
            'list'      => $list,
            'principal' => $this->article->getArticle(null, $categoryId),
            'title'     => $category->title,
            'count'     => $this->article->getCounts($categoryId),
            'skip'      => 10
        ]);
    }

    /*
     * ================== Obtener datos de artículo ==================
     */
    public function getArticle($article)
    {
        $this->notFoundUnless($article);

       // $this->visit->setVisit($article);
        return view('Frontend.Article.index', [
            'article'   => $article,
            'related'   => $this->article->getArticlesRelated($article, $article->ids->isEmpty() ? 'tags' : 'ids')
        ]);
    }

    /*
     * ================== Comprobar si el slug es una categoría o un artículo ==================
     */
    public function getArticleCategory($slug)
    {
        if ($subcategory = $this->category->findField('slug', $slug))
            return [
                'subcategory'   => $this->article->getArticlesForCategory($subcategory->id, 10,0),
                'id'            => $subcategory->id
            ];
        elseif ($article = $this->article->findField('slug', $slug))
            return ['article' => $this->article->getArticle($slug)];
        else
            $this->notFoundUnless(null);
    }

    /*
     * CARGAR MAS ANUNCIOS CLASIFICADOS
     * @param $skip, $slug
     * @Ajax Anuncios
     */
    public function moreArticles($categoryId, $skip)
    {
        $data = [
            'list'      => $this->article->getArticlesForCategory($categoryId,9,$skip),
            'category'  => $this->category->getCategory($categoryId),
            'skip'      => $skip
        ];

        return view('Frontend.Article.more',$data)->render();
    }

    /**
     * @param $skip
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function moreMinuteByMinute($skip) {
        $homeRepo = new HomeRepo();
        $minute = $homeRepo->getMoreMinute($skip);

        return view('Frontend.MinuteByMinute.more', ['minute' => $minute, 'skip' => $skip]);
    }
    
    /*
     * BUSCADOR DE NOTICIAS
     * @return vista RESULTADOS
     */
    public function search(ArticlesRepo $repo,SearchRequest $request,  HomeRepo $homeRepo)
    {
        $data = [
            'articles'   => $repo->search($request),
            'title'     => $request->get('search'),
            'modulesR'   => $homeRepo->getModulesR(),
        ];

        return view('Frontend.Article.results',$data);
    }

}
