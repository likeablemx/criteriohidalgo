<?php namespace App\Http\Frontend\Controllers;

use App\Http\Frontend\Repositories\FormsRepo;
use Illuminate\Http\Request;

class FormsController extends Controller
{
    /*
     * PRINCIPAL DE CONTACTOs
     * @return vista
     */
    public function index(FormsRepo $formRepo)
    {
	$data = [
            'form'        =>  $formRepo->getForm(),//obtenemos el formulario, con los inputs relacionados
            'title'         =>  'Contacto',
        ];
        session()->put('frontend.authorsLimit',''); // session de autores
	return view('Frontend.Forms.index',$data);
    }
    
    public function sendFormContact(FormsRepo $formRepo) {

        $redirect = $formRepo->saveMessage();

        return $redirect;
    }
    
    public function thanks($msj = '') {
        $data = [
            'title'         =>  'Gracias',
            'msj'           => 'Tus datos se han compartido correctamente.'
        ];
        return view('Frontend.Template.thanks',$data);
        
    }
}
