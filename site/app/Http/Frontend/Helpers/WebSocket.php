<?php namespace App\Http\Frontend\Helpers;

class WebSocket {
    /**
     * Conexion con web socket
        * Created by PhpStorm.
        * User: cesar
        * Date: 10/9/15
        * Time: 5:50 PM
        */

    /*
     * Enviar mensaje a cliente
     * 
     * @param Noticia
     *  @type Object Article
     * 
     */
    public static function sendMessage($article)
    {
        setlocale(LC_ALL, 'es_ES.UTF-8'); // formato de fecha y hora
        if(is_null($article->category->parent)){
            $link = $article->category->slug.'/'.$article->slug;
        }else{
            $link = $article->category->parent->slug.'/'.$article->category->slug.'/'.$article->slug;}
        /// Datos a enviar al cliente
        $entryData = [
            'topic'  => 'criterio.hidalgo.home',// Canal de transmicion, debe coincidir con el js del WS
            'title'     => $article->title,
            'category'  => $article->category->title,
            'date'      => strftime('%B %d, %Y', strtotime($article->created_at)),
            'time'      => strftime('%H:%M:%S %p', strtotime($article->created_at)),
            'id'        => $article->id,
            'link'      => $link,
        ];
        
    ///// Tiempo para visualizar en el frontend
            //sleep(2);  ////// Quitar esta linea

            $context    = new \ZMQContext();
            $socket     = $context->getSocket(\ZMQ::SOCKET_PUSH,'my pusher');
            $socket->connect("tcp://localhost:5555");
            $socket->send(json_encode($entryData));
    }
    
    /// funcion de prueba
    public static function test($msj)
    {
        error_reporting(E_ALL);
            $entryData = [
                'topic'  => 'criterio.hidalgo.home',// Canal de transmicion, debe coincidir con el js del WS
                'title'     => $msj,
                'category'  => $msj,
                'date'      => $msj,
                'time'      => $msj,
                'id'        => $msj,
                'link'      => $msj,
            ];
            sleep(2);

            $context    = new \ZMQContext();
            $socket     = $context->getSocket(\ZMQ::SOCKET_PUSH,'my pusher');
            $socket->connect("tcp://localhost:5555");
            $socket->send(json_encode($entryData));
    }
}
