<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Edition extends Model {

    protected $fillable = ['title','image','date_publishing','active'];
    
}
