<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model {

    protected $fillable = ['title','image','description','sub_category_id','user','phone','mail','active','validity'];
    protected $table    = 'ads';

    public function category()
    {
        return $this->belongsTo('App\Http\Entities\Sub_Classified','sub_category_id','id');
    }
}
