<?php namespace App\Http\Entities;

class Categories  extends \Eloquent
{
    protected $table        = 'categorizable';
    protected $fillable     = ['category_id'];
    public $timestamps      = false;

    public function categorizable()
    {
        return $this->morphTo();
    }

}