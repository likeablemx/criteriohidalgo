<?php namespace App\Http\Entities;

class GalleryElements extends \Eloquent
{
    protected $table    = 'gallery_elements';
    protected $guarded  = ['id'];
    public $timestamps  = false;

    public function gallery()
    {
        return $this->belongsTo('App\Http\Entities\Gallery', 'gallery_id');
    }
}