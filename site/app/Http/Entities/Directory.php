<?php namespace App\Http\Entities;

use Illuminate\Database\Eloquent\Model;

class Directory extends Model {

    protected $fillable = ['name','title','mail'];
    public $timestamps  = false;
    
}
