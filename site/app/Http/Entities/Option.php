<?php  namespace App\Http\Entities;

class Option  extends \Eloquent {
    protected $fillable = ['survey_id','sentence','counter','index'];
    public $timestamps  = false;
    
    public function survey() {
        return $this->belongsTo('App\Http\Entities\Survey');
    }
}
