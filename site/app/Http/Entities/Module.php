<?php namespace App\Http\Entities;

use App\Http\Backend\Traits\EntityCommonScopes;

class Module extends \Eloquent
{
    use EntityCommonScopes;

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo('App\Http\Entities\Category');
    }

    public function modulable()
    {
        return $this->morphTo();
    }

    public static function getPosition($sectionId) {
        $last = Module::where('section_id',$sectionId)
            ->orderBy('position','desc')
            ->first();

        if($last) {
            $position = $last->position + 1;
        } else {
            $position = 1;
        }

        return $position;
    }

    public function type()
    {
        return $this->belongsTo('App\Http\Entities\Type');
    }

    /*
     * ============ Galería de Imágenes ============
     */
    public function galleries()
    {
        return $this->MorphMany('App\Http\Entities\Gallery','imageable');
    }
}