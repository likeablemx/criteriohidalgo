<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Gallery;
use App\Http\Entities\GalleryElements;
use App\Http\Entities\Article;

// url demo     http://www.criteriolocal.euac/noticias/nueva-noticia-demo

class SliderRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'   => '',
        'featuring' => null,
        'inverse'   => false,
        'items'     => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Gallery();
    }

    /*
     * ================ Nombre del Panel de Control ================
     */
    public function setSectionName()
    {
        return 'Slider';
    }

    /*
     * ================== Listado y paginación de Usuarios ==================
     */
    public function paginate($filters)
    {
        
    }
    /*
     * ================ Guardar y Actualizar Usuarios ================
     */
    public function save($item, $request)
    {   
    }
    /*
     * ================ Eliminar registros ================
     */
    public function delete($idList)
    {   
        $elements = explode(',',$idList[0]);
        $entity = $this->setModel();
        $entity->destroy($elements);
    }
    /*
     * ================ Todas las galerias ================
     */
    public function getGallery()
    {
        return Gallery::where('name','MainSlider')->with('elements')->orderBy('position','asc')->get();
    }
    
    /*
     * ================ Guardar slider ================
     * @param   tipo de slider
     * @return  slider creado
     */
    public function setSlider($id)
    {
        $types = Gallery::where('name','MainSlider')->get();
        
        if($types->count() > 0)     // asignamos la posicion
            $position = $types->count() + 1;
        else
            $position = 1;
        
        // Creamos el slider
        $slider = Gallery::create([
            'name' => 'MainSlider',
            'directory' => $id,
            'position' => $position,  /// guardamos el tipo de slider
            'active' => 1
         ]);
        
        // Creamos los elementos del slider
            //Determinamos que tipo de slider es
        if($id == 1 || $id == 3)
            $num = 5;
        elseif($id == 2)
            $num = 4;
        elseif($id == 4)
            $num = 3;
        // $num indica cuantos elementos vamos a crear
        $data = [
            'type' => 'image',
            'gallery_id' => $slider->id
        ];
        for($x = 0; $x < $num; $x++){
            $element = new GalleryElements($data);
            $slider->elements()->save($element);
        }
        
        return $slider;
    }
    
    /*
     * ================ Extraer slider ================
     * @param   id de slider
     * @return  slider con elementos
     */
    public function getSlider($id)
    {
        return Gallery::where('id',$id)->with('elements')->first();
    }
    
    /*
     * ================ Asignar Url a slider ================
     * @param   request AJAX 
     * @return  Json con error, o url e imagen de nota
     */
    public function setUrl($request)
    {
        $id = $request->get("item");
        $url = $request->get("url");
        $entity = GalleryElements::find($id);
        
        foreach(array_filter(explode('/',$url)) as $value) $x[] = $value;  // Partimos la url, para almacenarla en un array
        
        $slug = array_pop($x);   // Obtenemos el ultimo elemento, el cual corresponde al slug
        
        $nota = Article::where('slug',$slug)
                ->with(['galleries' => function($elements){
                    $elements->with(['elements' => function($items){
                        $items->orderBy('position','asc');
                    }]);
                }],
                'category')
                ->first();
                
         // si la nota no exite se regresa un json con error       
        if(empty($nota)){
            $json = ['error' => 'La nota no existe'];
            return json_encode($json);
        }
         
        // si la nota no tiene imagen se asigna una generica
        if(empty($nota->galleries[0])){
            $json = ['image' => '/Backend/images/generica.jpg', 'url' => $url,'error' => ''];
            return json_encode($json);
        }
            // generamos la url de la imagen
        $image = '/'.$nota->galleries[0]->directory.'/'.$nota->galleries[0]->elements[0]->value;
        
        $entity->title = $nota->title;                                  // titulo
        $entity->link_text = $url;                                      // url
        $entity->link_href = $nota->category->image;                    // icono de la categoria
        $entity->position = $nota->id;                                  // id de la nota    
        $entity->subtitle = $this->datePublishing($nota->date_publishing);  // fecha de publicacion
        $entity->value = $image;                                        // ruta de la imagen
        $entity->save();
        $json = ['image' => $image, 'url' => $url,'error' => ''];
        return json_encode($json);
    }
    
    /*
     * 
     * ================ Ordenar Slider, cuando se ELIMINA un elemento ================
     *
     */
    public function reorder()
    {
        $galleries = Gallery::where('name','MainSlider')->orderBy('position','asc')->get();
        $aux = 1;
        foreach ($galleries as $gallery)
        {
            $gallery->position = $aux;
            $gallery->save();
            $aux++;
        }
    }
    /*
     * ============= Ordenar posiciones de elementos con Ajax =============
     * @param Elementos del slider
     */
    public function order($idList)
    {
        if(!empty($idList))
        {
            foreach($idList as $position => $id) $this->model->where('id', $id)->update(['position' => $position + 1]);
        }
    }
    /*
     * ============= Fecha de publicación =============
     */
    public function datePublishing($value)
    {
        $meses = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $value = explode('-',$value);
        $year = $value[0];
        $month = $value[1];
        $day = $value[2];
        $day = substr($day,0,2);
        if($month < 10)
            $month = str_replace('0','',$month);

        try {
            return $meses[$month] . ' ' . $day . ', ' . $year;
        }
        catch(\Exception $e)
        {
            dd($e,$value);
        }
        return $meses[$month].' '.$day.','.$year;

    }
}