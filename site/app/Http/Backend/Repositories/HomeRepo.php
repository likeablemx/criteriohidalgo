<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Module;
use App\Http\Entities\Category;

class HomeRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'   => '',
        'featuring' => null,
        'inverse'   => false,
        'items'     => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Module();
    }
    
    public function getIndex($extra = '')
    {
        return $this->setModel()->where('type_slug','blockLeft')->get();
    }
    
    public function getCategories()
    {
        return Category::all()->lists('title','id')->toArray();
    }

    /*
     * ================ Nombre del Panel de Control ================
     */
    public function setSectionName()
    {
        return 'HomeLeft';
    }

    /*
     * ================== Listado y paginación de Usuarios ==================
     */
    public function paginate($filters)
    {

    }
    /*
     * ================ Guardar y Actualizar Usuarios ================
     */
    public function save($item, $request)
    {   
        if(!$item) $item = $this->setModel();

        $item->fill($request->except('_token'))->save();

        return $item->id;
    }
    /*
     * ================ Eliminar registros ================
     */
    public function delete($idList)
    {

    }
}