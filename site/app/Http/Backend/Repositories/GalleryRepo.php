<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Gallery;

class GalleryRepo extends BaseRepo
{

    public function setModel()
    {
        return new Gallery();
    }

    public function setSectionName()
    {
        return "Galleries";
    }

    public function save($item, $request)
    {
        // TODO: Implement save() method.
    }

    public function paginate($filters)
    {
        // TODO: Implement paginate() method.
    }

    public function delete($idList)
    {
        // TODO: Implement delete() method.
    }

    /*
     * ================== Obtener Galería y Elementos ==================
     */
    public function getGallery($id)
    {
        return $this->model
            ->with(['elements' => function($elements){
                $elements->orderBy('position', 'asc');
            }])
            ->where('id', $id)
            ->first();
    }
}