<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\Statics;
use App\Http\Entities\Statics_element;
use App\Http\Backend\Helpers\Files as file;

class StaticsRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'   => '',
        'featuring' => null,
        'inverse'   => false,
        'items'     => 10,
    ];

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new Statics();
    }

    /*
     * ================ Nombre del Panel de Control ================
     */
    public function setSectionName()
    {
        return 'Statics';
    }

    /*
     * ================== Listado y paginación de Usuarios ==================
     */
    public function paginate($filters)
    {
        $list = new Statics_element();

        #----------- Buscar palabra clave -----------
        if(!empty($filters['keyword']))
        {
            $list = $list->where('static_id',2)->where(function($query) use($filters){
                $query->where('title','LIKE',"%{$filters['keyword']}%");
            });
        }

        $direction = ($filters['inverse'] == false) ? 'DESC' : 'ASC';

        $list = $list->where('static_id',2)->orderBy('title', $direction)->paginate($filters['items']);

        return $list;
    }
    /*
     * ================ Guardar y Actualizar Usuarios ================
     */
    public function save($item, $request)
    {   
        $slug       = $item->slug;

        foreach($request->input('id') as $num => $idElement) /// todos los campos del formulario
        {
                $image = $request->only("image.{$num}");
                
                $data['title']  = $request->input("title.{$num}");
                $data['link']   = $request->input("link.{$num}");
                $data['text']   = $request->input("text.{$num}");
                if($request->hasFile("image.{$num}"))
                {
                    $files = new file();
                    $file = $files->uploadFile([
                        'files'     => $image['image'],
                        'route'     => "images/statics_sections/{$slug}",
                        'entity'    => Statics_element::find($idElement)
                    ]);
                    if(!empty($file)) $data['image'] = $file;
                }
                else
                {
                    unset($data['image']);
                }
            
            $elements = Statics_element::find($idElement);
            $elements->fill($data);
            $item->elements()->save($elements);
        }
        return $item->id;
    }
    /*
     * ================ Eliminar registros ================
     */
    public function delete($idList)
    {
        return $this->model
            ->whereIn('id', $idList)
            ->delete();
    }
    
    /*
     * ================ Obtener seccion con elementos relacionadas ================
     */
    public function getElements($id)
    {
        $item = $this->model;

        if(!empty($id))
        {
            $item = $this->model->where('id',$id)->with('elements')->first();
            if(!$item) abort(404);
        }

        return $item;
    }
}