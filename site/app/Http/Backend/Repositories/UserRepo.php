<?php namespace App\Http\Backend\Repositories;

use App\Http\Entities\User;
use App\Http\Entities\Categories;

class UserRepo extends BaseRepo
{
    protected $defaultFilters = [
        'keyword'   => null,
        'role_id'   => null,
        'active'    => null,
        'inverse'   => false,
        'items'     => 10,
    ];

    private $editor = 3;

    /*
     * ================ Instanciar modelo ================
     */
    public function setModel()
    {
        return new User();
    }

    /*
     * ================ Nombre del la sección ================
     */
    public function setSectionName()
    {
        return 'Users';
    }

    /*
     * ================== Consultar registro por Id ==================
     */
    public function getItem($id)
    {
        $item       = null;
        $categories = [];

        if($id)
        {
            $item = $this->model->where('id', $id)->first();

            if(!$item) abort(404);

            #----------- Categorías asignadas al usuario -----------
            foreach($item->categories as $category) $categories[] = $category->category_id;

            $item->categories = $categories;
        }

        return $item;
    }

    /*
     * ================== Listado y paginación de registros ==================
     */
    public function paginate($filters)
    {
        $list = $this->model->with('role');

        #----------- Mostrar solo editores al Administrador -----------
        if(\Auth::user()->role->type == 'Admin')
        {
            $list->whereHas('role', function($role){
               $role->where('type', 'Editor');
            });
        }

        #----------- Buscar palabra clave -----------
        if(!empty($filters['keyword']))
        {
            $list = $list->where(function($query) use($filters){
                $query->where('name', 'LIKE',"%{$filters['keyword']}%");
            });
        }
        #----------- Buscar por tipo -----------
        !empty($filters['role_id']) ? $list = $list->where('role_id', $filters['role_id']) : null;

        #----------- Buscar por estatus -----------
        !empty($filters['active']) ? $list = $list->where('active', $filters['active'] == 'si' ? true : false) : null;

        $direction = ($filters['inverse'] == false) ? 'DESC' : 'ASC';

        $list = $list->orderBy('name', $direction)->paginate($filters['items']);

        return $list;
    }

    /*
     * ================ Guardar y actualizar registros ================
     */
    public function save($item, $request)
    {
        if(!$item) $item = $this->setModel(); else unset($item->assets); unset($item->categories);

        $item->fill($request->except('_token', 'categories'))->save();

        #---------- Guardar categorias asignadas a usuarios ----------
        $this->saveUserCategories($item, $request);

        return $item->id;
    }

    /*
     * ================ Eliminar registros ================
     */
    public function delete($ids)
    {
        if(!empty($ids))
        {
            $users = $this->model->whereIn('id', $ids)->get();

            foreach($users as $user)
                $user->categories()->delete();
                $user->delete();
        }
    }

    /*
     * =============== Datos completos de usuarios ===============
     */
    public function getUserData()
    {
        $assigned   = array();
        $user       = $this->model
            ->with('role', 'categories')
            ->find(\Auth::user()->id);

        if(\Auth::user()->role_id == $this->editor)
        {
            foreach($user->categories as $item)
                $assigned[] = $item->category_id;
        }

        $user->assigned = $assigned;

        return $user;
    }

    /*
     * ================== Guardar categorías asignadas ==================
     */
    public function saveUserCategories($user, $request)
    {
        $ids    = $request->get('categories');
        $role   = $request->get('role_id');

        #--------- Eliminar categorías asignadas anteriormente ---------
        $user->categories()->delete();

        if(!empty($ids) and $role == $this->editor)
        {
            foreach($ids as $id)
            {
                $user->categories()->save(new Categories([
                    'category_id'   => $id
                ]));
            }
        }
    }

}