<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Helpers\UploadHandler;
use App\Http\Backend\Helpers\Directory;
use App\Http\Backend\Repositories\GalleryElementRepo;
use App\Http\Backend\Repositories\GalleryRepo;

class GalleryController extends BaseController
{
    protected $elementRepo;

    function __construct(GalleryRepo $galleryRepo,GalleryElementRepo $elementRepo) {
        $this->repo        = $galleryRepo;
        $this->elementRepo = $elementRepo;
    }

    function DeleteElement($id = null)
    {
        $this->elementRepo->DeleteList(!empty($id) ? [$id] : \Input::get('multiselect'));

        return redirect()->back();
    }

    /*
     * =============== Cargar elemento ===============
     */
    function ElementAck($id)
    {
        $gallery = $this->repo->Find($id);

        if($gallery)
        {
            $fileName   = \Input::get('name');
            $element    = $this->elementRepo->SaveElement($fileName, $gallery);

            if($element)
            {
                $data = array(
                    'element'   => $element,
                    'gallery'   => $gallery,
                );
                return \View::Make('Backend.Galleries.element')->with($data);
            }
        }
        return false;
    }

    function GetBackUrl() {
        $url = \Request::Server('HTTP_REFERER');

        if(preg_match('%galeria%',$url) or preg_match('%tooltip%',$url)) {
            $url = \Session::Get('backend.galeria.back');
        } else {
            \Session::Put('backend.galeria.back',$url);
        }

        return $url;
    }

    /*
     * ================= Mostrar elementos de una galería =================
     */
    function ShowGallery($id, $type='')
    {
        $gallery = $this->repo->getGallery($id);

        if($gallery)
        {
            $config = \Config::get("gallery.{$type}");

            !$config ? $config = ['image','video','title','subtitle','link'] : null;

            $data = [
                'gallery'       => $gallery,
                'id'            => $id,
                'back'          => $this->GetBackUrl(),
                'galleryConfig' => $config,
            ];

            return \View::Make('Backend.Galleries.gallery')->with($data);
        }

        return redirect()->back();
    }

    /*
     * Guardar y cargar archivos
     */
    function SaveElement($id)
    {
        $gallery = $this->repo->Find($id);

        if($gallery)
        {
            // La clase UploadHandler realiza la carga de archivos fragmentados
            // enviados desde el cargador de archivos
            $UploadHandler = new UploadHandler([
                'upload_dir' => Directory::FixPath(storage_path($gallery->directory).'/'),
                'upload_url' => asset($gallery->directory).'/',
            ]);
        }
    }

    function UpdateElement($id) {
        $element = $this->elementRepo->Find($id);
        if($element) {
            $data = \Input::Except('_token');
            $this->elementRepo->Update($id,$data);
        }
        return \Redirect::Back();
    }

    public function addVideo($galleryId) {
        $data = \Input::except('_token');

        if($data['code']) {
            $this->elementRepo->saveVideo($galleryId,$data);
        }

        return redirect()->back();
    }

    /*
     * =============== Ordenar posiciones de elementos ===============
     */
    public function setOrder()
    {
        return $this->elementRepo->order(\Request::get('order'));
    }
}