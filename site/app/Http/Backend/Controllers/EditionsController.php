<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\EditionsRepo;
use App\Http\Backend\Requests\EditionRequest;

class EditionsController extends BaseController
{
    function __construct(EditionsRepo $repo)
    {
        parent::__construct($repo);
    }

    /*
     * =============== Panel ===============
     */
    public function getId($id)
    {
        \Session::put('mcpanel.author.id', $id);

        return redirect()->route('mcpanel.Author');
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getIndex($extra = [])
    {
        return parent::getIndex($extra);
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        $extra = [];
        return parent::getRegistro($id, $extra);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(EditionRequest $request, $id = null)
    {   
        return $this->doUpdate($request, $id);
    }

}
