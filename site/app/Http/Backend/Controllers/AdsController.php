<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\AdsRepo;
use Illuminate\Http\Request;

class AdsController extends BaseController
{
    function __construct(AdsRepo $repo)
    {
        parent::__construct($repo);
    }

    /*
     * =============== Panel ===============
     */
    public function getId($id)
    {
        \Session::put('mcpanel.author.id', $id);

        return redirect()->route('mcpanel.Author');
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getIndex($extra = [])
    {
        return parent::getIndex($extra);
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        $extra = [];
        return parent::getRegistro($id, $extra);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(Request $request, $id = null)
    {   
        return $this->doUpdate($request, $id);
    }

}
