<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\StaticsRepo;
use Illuminate\Http\Request;


class StaticsController extends BaseController 
{
    function __construct(StaticsRepo $section)
    {
        parent::__construct($section);
    }
    
    /*
     * =============== Panel ===============
     */
    public function getId($id)
    {
        \Session::put('mcpanel.statics.id', $id);

        return redirect()->route('mcpanel.Author');
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getIndex($extra = [])
    {
        $extra = ['content' => $this->notFoundUnless($this->repo->getList($id))];
        return parent::getIndex($extra);
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        $extra = [];
        $title  = 'Statics Edicion';
        $data   = [
            'id'    => $id,
            'item'  => $this->repo->getElements($id),
            'title' => ucfirst($title),
        ];

        if(is_array($extra)) $data = array_merge($data,$extra);

        return view("Backend.{$this->sectionName}.edit", $data)->withMessage(\Session::Get("{$this->sectionName}.message"));
    }
    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(Request $request, $id = null)
    {   
        return $this->doUpdate($request, $id);
    }
    /*
     * Modificar datos
    */
    public function modify(Request $request, Files $files, $id)
    {
        
    }
}
