<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\AuthorRepo;
use App\Http\Backend\Requests\AuthorRequest;
use Illuminate\Http\Request;

class AuthorsController extends BaseController
{
    function __construct(AuthorRepo $author)
    {
        parent::__construct($author);
    }

    /*
     * =============== Panel ===============
     */
    public function getId($id)
    {
        \Session::put('mcpanel.author.id', $id);

        return redirect()->route('mcpanel.Author');
    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getIndex($extra = [])
    {
        $extra = ['entity'=>1];
        return parent::getIndex($extra);
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {
        $extra = [];
        return parent::getRegistro($id, $extra);
    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(AuthorRequest $request, $id = null)
    {   
        return $this->doUpdate($request, $id);
    }

    public function complete(AuthorRepo $author, Request $request,$string)
    {
        if(\Request::ajax())
            return $author->getCompletes($string);
        else
            return null;

    }

}
