<?php namespace App\Http\Backend\Controllers;

use App\Http\Backend\Repositories\HomeRepo;
use App\Http\Backend\Requests\HomeRequest;

use App\Http\Frontend\Helpers\WebSocket;

class HomeLeftController extends BaseController
{
    function __construct(HomeRepo $home)
    {
        parent::__construct($home);
    }

    /*
     * =============== Panel ===============
     */
    public function getId($id)
    {

    }

    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getIndex($extra = [])
    {
        $filters    = $this->repo->getIndex();
        $categories = $this->repo->getCategories();
        //dd($categories);
        $data       = ['list'       => $filters,
                       'categories' => $categories  ];
        return view("Backend.HomeLeft.index", $data)
             ->with('message', \Session::get("{$this->sectionName}.message"));
    }
    
    /*
     * =============== Formulario para agregar y editar registro ===============
     */
    public function getRegistro($id = null, $extra = [])
    {

    }

    /*
     * =============== Guardar Datos ===============
     */
    public function postUpdate(HomeRequest $request, $id = null)
    {   
        $id = $this->repo->save($this->repo->getItem($id), $request);
        $this->repo->setMessage($this->sectionName, \Lang::get('general.saveData'), \Backend::msgType('success'));
        return redirect()->route('homeLeft');
    }
    public function test()
    {
        WebSocket::test('prueba');
    }
}
