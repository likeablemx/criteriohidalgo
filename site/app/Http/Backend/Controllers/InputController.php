<?php namespace App\Http\Backend\Controllers;

use App\Http\Entities\Module;
use App\Http\Backend\Repositories\InputRepo;
use App\Http\Backend\Requests\InputRequest;

class InputController extends BaseController {
    private $module;
    function __construct(InputRepo $repo) {
        $this->module = Module::where('type_slug','formulario_contacto')->first();
        /*        
        \Session::has('mcpanel.formularios.id')
            ? $this->module = Module::find(1)//\Session::get('mcpanel.formularios.id'))
            : redirect('mcPanel')
        ;
         * 
         */
        parent::__construct($repo);
    }

    public function postUpdate(InputRequest $request,$id=null) {
        $item = $this->repo->getItem($id);
        $id = $this->repo->save($item,$request);
        \Session::Flash("{$this->sectionName}.message",'Informacion Guardada');

        return redirect()->route('modulos.contacto.edicion',$id);
    }

    public function getEdicion($id = null, $extra = []) {
        $extra = [
            'types'     => $this->repo->typesList(),
            'module'    => $this->module,
            'last'      => $this->repo->GetLast($this->module),
        ];
        return parent::getRegistro($id, $extra);
    }

}