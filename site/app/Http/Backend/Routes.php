<?php

Route::group(['namespace' => 'Backend\Controllers', 'prefix' => 'mcPanel', 'middle'], function()
{
    /*
     * ====================== Acceso al Administrador ======================
     */
    Route::group(['namespace' => 'Auth'], function()
    {
        Route::get('login',     ['as' => 'user.login',      'uses'  => 'AuthController@index']);
        Route::post('access',   ['as' => 'user.access',     'uses'  => 'AuthController@postLogin']);
        Route::get('logout',    ['as' => 'user.logout',     'uses'  => 'AuthController@getLogout']);
        Route::get('recovery',  ['as' => 'user.recovery',   'uses'  => 'PasswordController@getEmail']);
        Route::post('reset',    ['as' => 'user.reset',      'uses'  => 'PasswordController@resetForEmail']);
    });
    /*
     * ====================== Administrador ======================
     */
    Route::group(['middleware' => ['Auth.Backend']], function()
    {
        /*
         * ====================== Filtros ======================
         */
        Route::post('filtros',  ['as' => 'filter',  'uses' => 'BaseController@filter']);
        Route::get('limpiar',   ['as' => 'clean',   'uses' => 'BaseController@clean']);

        Route::get('401',       ['as' => '401',     'uses' => 'DashboardController@error401']);
        
        Route::get('test',      ['as' => 'test',   'uses' => 'HomeLeftController@test']);
        /*
         * ====================== Dashboard ======================
         */
        Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

        /*
         * ====================== Autocomplete de Autores Ajax ======================
         */
        Route::get('autores/complete/{string}', 'AuthorsController@complete');
        /*
         * ====================== Accesos a SuperAdmin y Admin ======================
         */
        Route::group(['middleware' => ['Auth.Backend.Role:SuperAdmin|Admin']], function()
        {
        Route::get('banners/decode','CategoriesController@decode');
            /*
             * ====================== Usuarios ======================
             */
            Route::Controller('usuarios', 'UsersController',
                [
                    'getId'             => 'users.id',
                    'getIndex'          => 'users',
                    'getRegistro'       => 'users.edit',
                    'postUpdate'        => 'users.update',
                    'anyEliminar'       => 'users.delete',
                    'anyFiltrar'        => 'users.filter',
                    'postActive'        => 'users.active',
                ]
            );
            /*
             * ====================== Categorías ======================
             */
            Route::Controller('categorias', 'CategoriesController',
                [
                    'getId'             => 'categories.id',
                    'getIndex'          => 'categories',
                    'getRegistro'       => 'categories.edit',
                    'postUpdate'        => 'categories.update',
                    'anyEliminar'       => 'categories.delete',
                    'anyFiltrar'        => 'categories.filter',
                    'postActive'        => 'categories.active',
                ]
            );
        });
        /*
         * ====================== Notas ======================
         */
        Route::Controller('notas', 'ArticlesController',
            [
                'getId'             => 'articles.id',
                'getIndex'          => 'articles',
                'getRegistro'       => 'articles.edit',
                'getPreview'        => 'articles.preview',
                'postUpdate'        => 'articles.update',
                'anyEliminar'       => 'articles.delete',
                'anyFiltrar'        => 'articles.filter',
                'postStatus'        => 'articles.status',
            ]
        );
        /*
         * ============= Galerías =============
         */
        Route::Group(['prefix'=>'galeria'],function()
        {
            Route::post('actualizar-elemento/{id}', ['as' => 'gallery.elemento.actualizar',     'uses'  => 'GalleryController@UpdateElement'])->where('id','[0-9]+');
            Route::get('eliminar-elemento/{id}',    ['as' => 'gallery.elemento.eliminar',       'uses'  => 'GalleryController@DeleteElement'])->where('id','[0-9]+');
            Route::post('eliminar-elemento',        ['as' => 'gallery.elemento.eliminar.multi', 'uses'  => 'GalleryController@DeleteElement', 'before'=> 'csrf']);
            Route::post('agregar-video/{id}',       ['as' => 'gallery.agregar_video',           'uses'  => 'GalleryController@addVideo']);
            Route::get('ver/{id}/{type?}',          ['as' => 'gallery',                         'uses'  => 'GalleryController@ShowGallery',])->where('id','[0-9]+');
            Route::post('cargar/{id}',              ['as' => 'gallery.cargar',                  'uses'  => 'GalleryController@SaveElement',])->where('id','[0-9]+');
            Route::post('ack/{id}',                 ['as' => 'gallery.ack',                     'uses'  => 'GalleryController@ElementAck',])->where('id','[0-9]+');
            Route::post('order',                    ['as' => 'gallery.order',                   'uses'  => 'GalleryController@setOrder',]);
        });
        /*
         * ====================== Authors  ==========================
         */
        Route::Controller('autores', 'AuthorsController',
            [
                'getId'             => 'autores.id',
                'getIndex'          => 'autores',
                'getRegistro'       => 'autores.edit',
                'postUpdate'        => 'autores.update',
                'anyEliminar'       => 'autores.delete',
                'anyFiltrar'        => 'autores.filter',
            ]
        );
        /*
         * ====================== Accesos a SuperAdmin y Admin ======================
         */
        Route::group(['middleware' => ['Auth.Backend.Role:SuperAdmin|Admin']], function()
        {
            /*
             * ====================== Secciones estaticas  =======================
             */
            Route::Controller('secciones_es', 'StaticsController',
                [
                    'getId'             => 'statics.id',
                    'getIndex'          => 'statics',
                    'getRegistro'       => 'statics.edit',
                    'postUpdate'        => 'statics.update',
                ]
            );

            /*
             * ============ Home lado Izquierdo ==================
             */
            Route::Controller('homeLeft', 'HomeLeftController',
                [
                    'getId'             => 'homeLeft.id',
                    'getIndex'          => 'homeLeft',
                    'postUpdate'        => 'homeLeft.update',
                ]
            );

            /*
             * ====================== Directorio  ==========================
             */
            Route::Controller('directorio', 'DirectoryController',
                [
                    'getId'             => 'directorio.id',
                    'getIndex'          => 'directorio',
                    'getRegistro'       => 'directorio.edit',
                    'postUpdate'        => 'directorio.update',
                    'anyEliminar'       => 'directorio.delete',
                    'anyFiltrar'        => 'directorio.filter',
                ]
            );
            /*
             * ====================== Edicion impresa  ==========================
             */
            Route::Controller('edicion', 'EditionsController',
                [
                    'getId'             => 'edicion.id',
                    'getIndex'          => 'edicion',
                    'getRegistro'       => 'edicion.edit',
                    'postUpdate'        => 'edicion.update',
                    'anyEliminar'       => 'edicion.delete',
                    'anyFiltrar'        => 'edicion.filter',
                ]
            );

            /*
             * ====================== Anuncios clasificados  ==========================
             */
            Route::Controller('anuncios', 'AdsController',
                [
                    'getId'             => 'anuncios.id',
                    'getIndex'          => 'anuncios',
                    'getRegistro'       => 'anuncios.edit',
                    'postUpdate'        => 'anuncios.update',
                    'anyEliminar'       => 'anuncios.delete',
                    'anyFiltrar'        => 'anuncios.filter',
                    'postStatus'        => 'anuncios.status',
                ]
            );

            /*
             * ====================== Encuesta  =======================
             */
            Route::Controller('encuestas', 'SurveyController',
                [
                    'getId'             => 'surveys.id',
                    'getIndex'          => 'surveys',
                    'getRegistro'       => 'surveys.edit',
                    'postUpdate'        => 'surveys.update',
                    'anyEliminar'       => 'surveys.delete',
                    'anyFiltrar'        => 'surveys.filter',
                ]
            );

           /*
            * ============= Formularios =============
            */
           Route::Controller('formularios','FormController',[
               'getId'                     => 'forms.id',
               'getIndex'                  => 'forms',
               'postUpdate'                => 'forms.update',
               'getReporte'                => 'forms.reporte',
           ]);

           /*
            * ============= Mensajes de contacto =============
            */
           Route::Controller('responses','ResponseController',[
               'getId'             => 'mensajes.id',
               'getIndex'          => 'mensajes',
               'getRegistro'       => 'mensajes.details',
               'postUpdate'        => 'mensajes.update',
               'anyFiltrar'        => 'mensajes.filter',
               'anyEliminar'       => 'mensajes.delete',
           ]);

           /*
            * ============= Slider principal =============
            */
            Route::Group(['prefix'=>'slider'],function()
            {
                Route::get('/',             ['as' => 'slider',       'uses'  => 'SliderController@getIndex']);
                Route::get('view',          ['as' => 'slider.view',  'uses'  => 'SliderController@getView']);
                Route::post('store',        ['as' => 'slider.store', 'uses'  => 'SliderController@setUrlSlider']);
                Route::post('update',       ['as' => 'slider.update','uses'  => 'SliderController@postUpdate']);
                Route::post('eliminar/{id}',['as' => 'slider.delete','uses'  => 'SliderController@anyEliminar']);
                Route::post('setOrder',     ['as' => 'slider.order', 'uses'  => 'SliderController@setOrder']);
            });

            /*
             * ============ Home lado Izquierdo ==================
             */
            Route::Controller('homeLeft', 'HomeLeftController',
                [
                    'getId'             => 'homeLeft.id',
                    'getIndex'          => 'homeLeft',
                    'postUpdate'        => 'homeLeft.update',
                ]
            );

            /*
             * ====================== Directorio  ==========================
             */
            Route::Controller('directorio', 'DirectoryController',
                [
                    'getId'             => 'directorio.id',
                    'getIndex'          => 'directorio',
                    'getRegistro'       => 'directorio.edit',
                    'postUpdate'        => 'directorio.update',
                    'anyEliminar'       => 'directorio.delete',
                    'anyFiltrar'        => 'directorio.filter',
                ]
            );
           /*
            * ============= Modulos =============
            */
            Route::group(['prefix' => 'modulos'], function()
            {
                Route::get('lista/{id}',                ['as' => 'module.list',         'uses'=>'ModuleController@getList']);
                Route::post('add/{id}',                 ['as' => 'module.add',          'uses'=>'ModuleController@addModule']);
                Route::post('update/{id}',              ['as' => 'module.update',       'uses'=>'ModuleController@updateModule']);
                Route::get('delete/{id}',               ['as' => 'module.delete',       'uses'=>'ModuleController@deleteModule']);
                Route::post('delete',                   ['as' => 'module.delete.multi', 'uses'=>'ModuleController@deleteModule', 'before'=> 'csrf']);
                Route::get('visibility/{id}',           ['as' => 'module.visibility',   'uses'=>'ModuleController@toogleVisibility']);
                Route::post('order',                    ['as' => 'module.order',        'uses'=>'ModuleController@setOrder']);

                Route::group(['prefix'=>'contacto'],function()
                {
                    Route::get('edicion/{id?}',     ['as'=>'modulos.contacto.edicion',      'uses'=>'InputController@getEdicion']);
                    Route::post('actualizar/{id?}', ['as'=>'modulos.contacto.actualizar',   'uses'=>'InputController@postUpdate']);
                    Route::any('eliminar/{id}',     ['as'=>'modulos.contacto.eliminar',     'uses'=>'InputController@anyEliminar']);
                });

                Route::group(['prefix'=>'ultimas-noticias'],function()
                {
                    Route::post('seleccionar/{moduleId}',   ['as'=>'modulos.noticias.seleccionar',  'uses'=>'ModuleController@selectLastPostsSection']);
                });
            });

            /*
             * ====================== Anuncios clasificados ======================
             */
            Route::group(['prefix' => 'clasificados'], function()
            {
                Route::get('/', 'AdsController@show');
                Route::get('detalles/{id}',  ['as' => 'AdsDetails', 'uses' => 'AdsController@details']);
                Route::get('elim/{id}',  	 ['as' => 'AdsElim',    'uses' => 'AdsController@elim']);
                Route::post('tarea',         ['as' => 'taskAds',    'uses' => 'AdsController@task']);
            });
        });
    });

});


        

        



