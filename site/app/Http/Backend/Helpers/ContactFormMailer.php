<?php
namespace App\Http\Backend\Helpers;


class ContactFormMailer {
    public static function autoResponder($form,$data) {
        $email = $form->getMainEmailField();
        if($email) {
            \Mail::send('Frontend.Forms.formulario.respuestas.usuario',['response'=>$form->response],function($message) use($email,$form,$data) {
                $message->from('contacto@criteriohidalgo.com','Criterio Hidalgo');
                $message->to($data[$email->slug]);
                $message->subject($form->subject);
            });
        }
    }

    public static function notify($form,$data) {
        $dataMail = [
            'data'      => $data,
            'form'      => $form,
            //'section'   => $form->module[0]->section,
        ];

        \Mail::send('Frontend.Forms.formulario.respuestas.administrador',$dataMail,function($message) use ($form) {
            $message->subject('Mensaje de contacto');
            $message->from('contacto@criteriohidalgo.com','Criterio Hidalgo');

            $addresses = explode(',',$form->to);
            foreach($addresses as $address) $message->to($address);

            $message->bcc('uzziel.alonso@masclicks.com.mx');
        });
    }
}