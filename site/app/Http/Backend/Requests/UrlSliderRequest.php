<?php namespace App\Http\Backend\Requests;

//use App\Http\Frontend\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;

class UrlSliderRequest extends FormRequest {

    public function authorize()
    {
        return true;
    }

    protected $generalRules    = [
        'url'               => 'required|max:255|url'
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return $this->generalRules;
    }

}
