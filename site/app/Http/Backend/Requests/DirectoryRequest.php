<?php namespace App\Http\Backend\Requests;

use Illuminate\Routing\Route;

class DirectoryRequest extends Request
{
    protected $rules = [
        'name'      => 'required|max:255',
        'title'     => 'required|max:255',
        'mail'      => 'email|max:255'
    ];

    public function rules(Route $route)
    {
        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 