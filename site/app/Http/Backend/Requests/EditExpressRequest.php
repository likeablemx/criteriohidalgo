<?php namespace App\Http\Backend\Requests;

//use App\Http\Frontend\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;

class EditExpressRequest extends FormRequest {

    public function authorize()
    {
        return true;
    }

    protected $generalRules    = [
        'text'               => 'required|max:255'
    ];
    protected $galleryRules    = [
        'text'               => 'min:2|max:255'
    ];
    protected $productRules    = [
        'text'               => 'required|max:255|numeric'
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request->get("source") == '1' || $request->get("source") == '2')
            return $this->generalRules;
        elseif($request->get("source") == '3')
            return $this->galleryRules;

    }

}
