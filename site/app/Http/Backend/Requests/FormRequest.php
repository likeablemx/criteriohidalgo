<?php
namespace App\Http\Backend\Requests;


class FormRequest extends Request {
    public function authorize() {
        return true;
    }

    public function rules() {
        $rules = [
            'to'        => ['required','email_list'],
            'response'  => ['required'],
            'subject'   => ['required'],
            'submit'    => ['required'],
        ];

        return $rules;
    }
}