<?php namespace App\Http\Backend\Requests;

class CategoryRequest extends Request
{
    protected $rules = [
        'title'     => 'required|max:255',
        'parent_id' => 'required'
    ];

    public function rules()
    {
        $this->get('parent_id') == 'root'
            ? $rules['parent_id'] = null
            : null
        ;

        return $this->rules;
    }

    public function authorize()
    {
        return true;
    }
} 