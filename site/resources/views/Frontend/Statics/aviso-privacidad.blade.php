@extends('master')
@section('title'){{ $title }}@endsection
@section('content')


<main>

    <div id="module_tittle_category" class="author_tittle">
        <div id="content">
            <h1>{!! $section->elements[0]->title !!}</h1>
        </div>
    </div>

    <div id="container">
        <div class="row">
        
            <div id="static" class="col-xs-12 col-sm-10">
                <h2>{!! $section->elements[0]->title !!}</h2>
                
                <hr>
                
                <!--CONTENIDO-->
                <div>
                    <h3>{!! $section->elements[1]->title !!}</h3>
                    
                    {!! $section->elements[1]->text !!}
                    
                    <h3>{!! $section->elements[2]->title !!}</h3>
                    
                    {!! $section->elements[2]->text !!}
                    <!--
                    <p>Lorem ipsum <span>dolor</span> sit amet, consectetur adipiscing elit. Integer ac vulputate odio, sit amet consequat justo. Suspendisse quam velit, viverra at tellus sed, porttitor imperdiet ligula. Nam consectetur diam eros, a cursus ex tristique in. Maecenas vitae urna suscipit, vehicula lorem eget, <span>tristique</span>felis. Morbi eros lacus, dapibus a blandit eu, mollis at lorem. Suspendisse potenti. Donec ornare sollicitudin mi vehicula ultrices. Sed maximus id urna a ullamcorper. <span>Proin non quam at sem sodales lobortis eu a nibh.</span></p>

                    <p>Nullam maximus ac tortor in mattis. Pellentesque egestas volutpat odio, vitae tristique leo sagittis ac. Maecenas bibendum enim nisl, id convallis augue facilisis vitae. Maecenas ac mi ante. Vestibulum eleifend feugiat mi eget volutpat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed nec purus dictum, lobortis velit eu, congue odio. In consectetur volutpat purus, a lobortis dui condimentum a.</p>

                    <p>Phasellus eu sem dolor. <span>Quisque pretium tortor sit amet turpis interdum volutpat. Etiam fringilla mi nisi, sed sollicitudin nisl aliquam eget.</span> Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse potenti. Phasellus mattis vitae urna sit amet volutpat. Curabitur sodales nunc aliquam, feugiat nunc id, vestibulum magna.</p>
                    
                    <ul>
                        <li><span>ITEM 1</span></li>
                        <li><span>ITEM 1</span></li>
                        <li><span>ITEM 1</span></li>
                        <li><span>ITEM 1</span></li>
                        <li><span>ITEM 1</span></li>
                        <li><span>ITEM 1</span></li>
                        <li><span>ITEM 1</span></li>
                        <li><span>ITEM 1</span></li>
                    </ul>
                    
                    <h3>Titulo 2</h3>
                    <p>Sed vel <span>metus</span> nec ante pretium dictum vitae vel velit. Etiam accumsan arcu at lacus finibus porttitor. Maecenas a urna odio. Suspendisse suscipit metus nisi, eget condimentum leo egestas sed. Praesent imperdiet, eros ut blandit finibus, velit dui rhoncus ligula, eget aliquet leo massa quis arcu. Pellentesque ut quam vestibulum, auctor ex vel, sagittis ipsum. Nullam rhoncus diam eget nisi tempus, in ultricies leo elementum. Nunc sollicitudin nisl sed nulla posuere eleifend pellentesque at nulla. Pellentesque pellentesque dignissim eleifend. Nunc quis metus ex. <span>Nullam hendrerit scelerisque ipsum, id varius ante vestibulum eget. Praesent ipsum dolor, aliquet sed mattis non, ultricies eget lacus. Curabitur</span> sagittis rhoncus velit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>

                    <p>Nullam et velit sed mauris mattis sodales id vitae neque. Donec ut lectus turpis. Praesent ultrices in nibh vel efficitur. Fusce nisi orci, auctor a massa at, porta blandit mauris. Proin rutrum volutpat nunc, nec dictum nibh. Pellentesque ullamcorper elit eu euismod malesuada. Nullam a leo quam. Vivamus tincidunt tincidunt faucibus.</p>
                -->
                </div>
                
            </div>
        </div>
    </div>
    

</main>

@include('Frontend.Template.footer')
    
@endsection