<!-- MODULO DE GALERIA -->
<div id="module_gallery">
    <?php $first = array_shift($recent[2]);?>
    <div class="moduleTop">
        @if(!empty($first['category']))
        <h2>{{ $first['category'] }}</h2>
        @endif
    </div>

    <div id="galleryContainer">
        <!-- IMAGEN PRINCIPAL  -->
        @if(!empty($first['subcategory']))
            <?php $route = $first['subcategory'] ? [$first['category'],$first['subcategory'],$first['slug']] : [$first['category'],$first['slug']]; ?>
        @elseif(!empty($first['category']))
            <?php $route = [$first['category'],$first['slug']]; ?>
        @else
        <?php $route = []; ?>
        @endif
        <a class="mainImageHover" href="{{ route('notes', $route) }}">
            <div class="mainImage">
                <div class="cropImg">
                    @if(empty($first['img']))
                        <img src="/Backend/images/generica.jpg" alt="car1">
                    @else
                        <img src="{{$first['dir'] }}/{{ $first['imgFirst'] }}" alt="car1">
                    @endif
                </div>
                <section>
                  {{--  <h2>{{ strtoupper($first['title']) }}</h2>
                    <p><span class="calendar"></span>{{ $first['date'] }}</p>
                --}}
                </section>
            </div>
        </a>
{{--
        <!-- IMAGENES SECUNDARIAS  -->
        <div id="gallerySec">
            <div class="row">
                @foreach($recent[2] as $item)
                <div class="col-xs-12 col-sm-6">
--}}

    {{--                <a class="gallerySecHover" href="{{ route('notes', $route) }}">
                        <div class="cropImg">
                            @if($item['img'] == null)
                                <img src="/Backend/images/generica.jpg" alt="car1">
                            @else
                                <img src="{{$item['dir'] }}/{{ $item['img'] }}" alt="car1">
                            @endif
                        </div>
                        <div class="info">
                            <h3>{{ $item['title'] }}</h3>
                            <span>{{ $item['date'] }}</span>
                            <p>{{ $item['intro'] }}</p>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    --}}
    </div>

</div>
