<!-- MODULO DE NOTICIAS DESTACADAS -->
<div id="module_main">
   
    <!-- SLIDER DE NOTICIAS DESTACADAS -->
    <div class="moduleTop">
        <h2>Destacado</h2>
        <div>
            <a id="prevNew" class="prev" href="#">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            </a>
            <a id="nextNew" class="next" href="#">
             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            </a>
        </div>
    </div>
    
    <div class="row" id="newCarrouselContainer">
        <div id="newCarrousel">
           <!-- SLIDER -->
           @foreach($featuring as $slider)
           <div class="slide">
                <!-- NOTICIA PRINCIPAL -->
                <?php $first = array_shift($slider) ?>
                <?php $route = $first['subcategory'] ? [$first['category'],$first['subcategory'],$first['slug']] : [$first['category'],$first['slug']]; ?>
                <a class="mainImageHover" href="{{ route('notes', $route) }}">
                   <div class="mainImage">
                    <div class="cropImg">
                        @if($first['img'] == '')
                            <img src="/Backend/images/generica.jpg" alt="car1">
                        @else
                            <img src="{{$first['dir'] }}/{{ $first['imgFirst'] }}" alt="car1">
                        @endif
                    </div>
                    <span class="category"><img src="../images/categories/{{ $first['icon'] }}" alt="Categoria"></span>
                    <section>
                        <h2>{{ $first['title'] }} </h2>
                        <p><span class="calendar"></span> {{ $first['date'] }}</p>
                    </section>
                </div>
               </a>
                
                <!-- NOTICIAS DESTACADAS -->
                <div class="news">
                    @foreach($slider as $item)
                    <?php $route = $item['subcategory'] ? [$item['category'],$item['subcategory'],$item['slug']] : [$item['category'],$item['slug']]; ?>
                    <a class="newsHover" href="{{ route('notes', $route) }}">
                       <section class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="cropImg">
                                @if($item['img'] == '')
                                    <img src="/Backend/images/generica.jpg" alt="car1">
                                @else
                                    <img src="{{$item['dir'] }}/{{ $item['img'] }}" alt="car1">
                                @endif
                            </div>
                            <span class="category"><img src="../images/categories/{{ $item['icon'] }}" alt="Categoria"></span>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="info">
                                <h3>{{ $item['title'] }}</h3>
                                <span>{{ $item['date'] }}</span>
                                <p>{{ $item['intro'] }}</p>
                            </div>
                        </div>
                    </section>
                    </a>
                   @endforeach
                </div>
            </div>
            @endforeach           
        </div>
    </div>    
    
</div>