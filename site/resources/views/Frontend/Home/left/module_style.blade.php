<!-- MODULO DE LIFE STYLE -->
<div id="module_style">
  
   <!-- NOTAS PRINCIPALES -->
    <div id="styleMain" class="row slide">
        <div class="col-xs-12">
            <div class="row">
                <?php $first = array_shift($recent[1]);?>
                <div id="main" class="col-xs-12 col-sm-8">    
                    <?php $route = $first['subcategory'] ? [$first['category'],$first['subcategory'],$first['slug']] : [$first['category'],$first['slug']]; ?>
                    <a class="styleHover" href="{{ route('notes', $route) }}">
                        <div class="cropImg">
                            @if($first['img'] == null)
                                <img src="/Backend/images/generica.jpg" alt="main">
                            @else
                                <img src="{{$first['dir'] }}/{{ $first['imgFirst'] }}" alt="main">
                            @endif
                        </div>
                            <span class="category"><img src="../images/categories/{{ $first['icon'] }}" alt="Categoria"></span>
                        <section>
                            <h2>{{ strtoupper($first['title']) }}</h2>
                            <p><span class="calendar"></span>{{ $first['date'] }}</p>
                        </section>
                   </a>
                </div>
                <?php $next = array_shift($recent[1]);?>
                <div class="subMains col-xs-12 col-sm-4">
                    <?php $route = $next['subcategory'] ? [$next['category'],$next['subcategory'],$next['slug']] : [$next['category'],$next['slug']]; ?>
                    <a class="styleHover" href="{{ route('notes', $route) }}">
                        <div class="cropImg">
                        @if($next['img'] == null)
                            <img src="/Backend/images/generica.jpg" alt="main">
                        @else
                            <img src="{{$next['dir'] }}/{{ $next['img'] }}" alt="main">
                        @endif
                        </div>
                            <span class="category"><img src="../images/categories/{{ $next['icon'] }}" alt="Categoria"></span>
                        <section>
                            <h2>{{ strtoupper($next['title']) }}</h2>
                            <p><span class="calendar"></span>{{ $next['date'] }}</p>
                        </section>
                    </a>
                </div>
                <div class="subMains col-xs-12 col-sm-4">
                    <?php $last = array_shift($recent[1]);?>
                    <?php $route = $last['subcategory'] ? [$last['category'],$last['subcategory'],$last['slug']] : [$last['category'],$last['slug']]; ?>
                        <a class="styleHover" href="{{ route('notes', $route) }}">
                        <div class="cropImg">
                        @if($last['img'] == null)
                            <img src="/Backend/images/generica.jpg" alt="main">
                        @else
                            <img src="{{$last['dir'] }}/{{ $last['img'] }}" alt="main">
                        @endif
                        </div>
                            <span class="category"><img src="../images/categories/{{ $last['icon'] }}" alt="Categoria"></span>
                        <section>
                            <h2>{{ strtoupper($last['title']) }}</h2>
                            <p><span class="calendar"></span>{{ $last['date'] }}</p>
                        </section>
                        </a>    
                </div>
            </div>
        </div>
    </div>
    
    <!-- NOTAS SECUNDARIAS -->
    <div id="styleSec" class="row">
        @foreach($recent[1] as $item)
        <div class="col-xs-6 col-sm-3">
            <?php $route = $item['subcategory'] ? [$item['category'],$item['subcategory'],$item['slug']] : [$item['category'],$item['slug']]; ?>
            <a class="styleHover" href="{{ route('notes', $route) }}">
                <div class="cropImg">
                    @if($item['img'] == null)
                        <img src="/Backend/images/generica.jpg" alt="Random">
                    @else
                        <img src="{{$item['dir'] }}/{{ $item['img'] }}" alt="Random">
                    @endif
                </div>
                <div class="info">
                    <h3>{{ $item['title'] }}</h3> 
                    <p>{{ $item['date'] }}</p> 
                </div>
            </a>    
        </div>
        @endforeach
    </div>
    
    
</div>