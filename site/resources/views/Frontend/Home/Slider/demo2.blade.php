<!-- CONTENEDOR DE PLANTILLA -->
<section id="template2">

        <div id="carrousel">
        <!-- PRIMER SLIDER -->
        <?php $aux = 0; ?>
        @foreach($slider as $item)
        <?php $aux++; ?>
        <div id="sports" class="row slider">
           @foreach($item['elements'] as $element)
           <a class="hoverBox" href="{{ $element['link_text'] }}">
               <div class="col-sm-3 item">
                    <div class="cropImg">
                        @if($element['value'] == '')
                            <img src="/Backend/images/generica.jpg" alt="other">
                        @else
                            <img src="{{ $element['value'] }}" alt="other">
                        @endif 
                    </div>
                    <span class="category"><img src="../images/categories/{{ $element['link_href'] }}" alt="Categoria"></span>
                    <section>
                        <h3>{{ $element['title'] }}</h3>
                        <p><span class="calendar"></span> {{ $element['subtitle'] }}</p>
                    </section>
               </div>
           </a>
           @endforeach
        </div>
        @endforeach
        <!-- SEGUNDO SLIDER -->
        

    </div>

      <!-- CONTROLES -->
     @if($aux > 1)
        @include('Frontend.Home.Slider.controles')
     @endif   

</section>