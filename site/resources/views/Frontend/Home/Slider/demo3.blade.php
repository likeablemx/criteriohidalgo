<section id="template3">

<div id="carrousel">
       <?php $aux = 0; ?>
   <!-- PRIMER SLIDER -->
   @foreach($slider as $item)
   <?php $aux++; ?>
    <div id="sports" class="row slider">
     
      <!-- NOTA DESTACADA -->
      <?php $first = array_shift($item['elements']) ?>
      <a class="hoverBox" href="{{ $first['link_text'] }}">
       <div class="col-sm-6 item main">
            <div class="cropImg">
                
                    @if($first['value'] == '')
                         <img src="/Backend/images/generica.jpg" alt="main">
                    @else
                        <img src="{{ $first['value'] }}" alt="main">
                    @endif
            </div>
           <span class="category"><img src="../images/categories/{{ $first['link_href'] }}" alt="Categoria"></span>
            <section>
                <h3>{{ $first['title'] }}</h3>
                <p><span class="calendar"></span> {{ $first['subtitle'] }}</p>
            </section>
       </div>
        </a>
      
        <!-- NOTA DESTACADA -->
       <?php $next = array_shift($item['elements']) ?>
       <a class="hoverBox" href="{{ $next['link_text'] }}">
           <div class="col-sm-6 item main">
            <div class="cropImg">         
                    @if($next['value'] == '')
                        <img src="/Backend/images/generica.jpg" alt="main">
                    @else
                        <img src="{{ $next['value'] }}" alt="main">
                    @endif
            </div>
            <span class="category"><img src="../images/categories/{{ $next['link_href'] }}" alt="Categoria"></span>
            <section>
                <h3>{{ $next['title'] }}</h3>
                <p><span class="calendar"></span> {{ $next['subtitle'] }}</p>
            </section>
           </div>
        </a>

       <!-- NOTAS SECUNDARIAS -->
       @foreach($item['elements'] as $element)
       <a class="hoverBox" href="{{ $element['link_text'] }}">
        <div class="col-sm-4 other item">
            <div class="cropImg">
                @if($element['value'] == '')
                    <img src="/Backend/images/generica.jpg" alt="other">
                @else
                    <img src="{{ $element['value'] }}" alt="other">
                @endif    
            </div>
            <span class="category"><img src="../images/categories/{{ $element['link_href'] }}" alt="Categoria"></span>
            <section>
                <h3>{{ $element['title'] }}</h3>
                <p><span class="calendar"></span> {{ $element['subtitle'] }}</p>
            </section>
        </div>
        </a>
       @endforeach
    </div>
   @endforeach
   
</div>

      <!-- CONTROLES -->
     @if($aux > 1)
        @include('Frontend.Home.Slider.controles')
     @endif   

</section>