@if($survey)
    @if(empty($voted))
        <div class="module_test" id='renderSurvey'>
            {!! Form::open(['method' => 'POST' ]) !!}
                <h2 class="rightTitle">Encuesta</h2>
                <div class="test">
                    <h3>{{ $survey->sentence }}</h3>
                    @foreach($survey->options as $option)
                        @if($option->sentence)                                 
                        <div>{!! Form::radio('answer', 1, true, ['id' => $option->id,'class' => 'options green']) !!}
                            {!! Form::label($option->id,$option->sentence) !!}
                        </div>    
                        @endif
                    @endforeach
                    <input class="submit btnVotar" type="button" value="RESPONDER">
                </div>
            {!! Form::close() !!}
        </div>
     @else
        @include('Frontend.Survey.results',$survey)
    @endif 
@endif 
          
   