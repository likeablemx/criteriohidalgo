@foreach($minute as $item)
    <a href="/{{ $item['link'] }}" class="item_minute">
        <div class="row">
            <div class="col-xs-4">
                <p class="date">{{ $item['date'] }}</p>
                <p class="time">{{ $item['time'] }}</p>
            </div>
            <div class="col-xs-1">
                <span class="bullet"></span>
            </div>
            <div class="col-xs-7">
                <p class="sub">{{ $item['category'] }}</p>
                <p class="title">{{ $item['title'] }}</p>
            </div>
        </div>
        <input type="hidden" class="lastTime" data-lasttime="{{ $item['dateP']  }}">
    </a>
@endforeach