<?php extract(\Frontend::moduleLastArticles($item->params)) ?>
<div class="module_news" style="max-height: 720px; overflow-y: auto; overflow-x: hidden">
    @if(!empty($content))
    <h2 class="rightTitle">{{ $title->title }}</h2>
        @if($content->count() > 0)
            @foreach($content as $item)
            <!-- NOTA -->

            <?php $gallery = $item->galleries ? $item->galleries->first() : null; ?>
        <?php $route = $item->category->parent ? [$item->category->parent->slug,$item->category->slug,$item->slug] : [$item->category->slug,$item->slug]; ?>
        <a class="module_newsHover" href="{{ route('notes', $route) }}">
            <div class="row">
                <div class="col-xs-6 col-sm-3 col-md-6">
                    <div class="cropImg">
			@if($gallery != null)
                        @if(!$gallery->elements->isEmpty())
                            <?php $val = $gallery->elements->first()->subtitle ? $gallery->elements->first()->subtitle : $gallery->elements->first()->value; ?>
                            <img src="{{ asset("{$gallery->directory}/{$val}") }}" alt="{{ $item->title }}">
                        @else
                            <img src="/Backend/images/generica.jpg" alt="Generica">
                        @endif
			@endif
                    </div>
                </div>
                <div class="col-xs-6 col-sm-9 col-md-6">
                    <h3>{!! $item->title !!}</h3>
                    <span>{!! \Frontend::datePublishing($item->date_publishing) !!}</span>
                </div>
            </div>
        </a>
            @endforeach
        @endif
    @else
        <hr>
        Notas Recientes: No hay notas que mostrar.
        <hr>
    @endif
</div>
