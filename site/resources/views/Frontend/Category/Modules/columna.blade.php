<?php extract(\Frontend::moduleGallery($item->params)) ?>
<div class="module_column">
    @if(!empty($content))
        <h2 class="rightTitle">{{ $title }}</h2>
        @if(!$content->elements->isEmpty())
            <?php $val = $content->elements[0]->subtitle ? $content->elements[0]->subtitle : $content->elements[0]->value; ?>
        <div class="grid column">
            <figure class="effect-ming">
                <img src="{{ asset("{$content->directory}/{$val}") }}" alt="Columna">
                <figcaption>
                    <p>Ver columna <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> </p>
                    <a href="{{ $link }}">Ver más</a>
                </figcaption>
            </figure>
        </div>
        @endif
    @else
        <hr>
        Columna: No existe la nota.
        <hr>
    @endif
</div>