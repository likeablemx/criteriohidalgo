<?php extract(\Frontend::moduleGallery($item->params)) ?>

@if(!empty($content))
<h2 class="rightTitle">{{ $title }}</h2>

<a class="hoverGallery" href="{{ route('notes', \Frontend::getRoute($content->imageable, $content->imageable->slug)) }}">
    <div class="module_bottom">
        @if(!$content->elements->isEmpty())
            <div class="row">
                <div class="mainImage col-xs-12">
                    <div class="cropImg">
                        <img src="{{ asset("{$content->directory}/{$content->elements[0]->value}") }}" alt="Top">
                    </div>
                </div>
            </div>

            <?php unset($content->elements[0]) ?>
            @if($content->elements->count() > 0)
                <div class="row">
                @foreach($content->elements as $aux => $element)
                    @if($aux < 4)
                            <div class="smallImage col-xs-4">
                                <div class="cropImg">
                                    <img src="{{ asset("{$content->directory}/{$element->value}") }}" alt="{{ $element->title }}">
                                </div>
                            </div>

                    @endif
                @endforeach
                </div>
            @endif
        @endif
    </div>
</a>
@else
    <hr>
    Galería: No existe la nota.
    <hr>
@endif