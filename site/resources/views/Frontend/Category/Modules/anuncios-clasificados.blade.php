<?php extract(\Frontend::moduleAds()) ?>
<div class="module_classified">
    <div class="advertise">
        <a href="/anuncios-clasificados/publicar"><img src="{{ asset("img/right/banner-clasificados.jpg") }}" alt="Banner"></a>
    </div>
    
    <div class="adds">
        <div class="row">
            <div class="col-xs-12">
                <h2>{{ $title }}</h2>
            </div>
        </div>
        
        <!-- ANUNCIO -->
        @foreach($content as $item)
        <div class="row add">
            <a href="/anuncios-clasificados/detalles/{{$item->id}}">
            <div class="col-xs-4 col-sm-2 col-md-3">
                <div class="cropImg">
                    <img src="{{ asset("images/ads/{$item->image}") }}" alt="Add">
                </div>
            </div>
            <div class="col-xs-8 col-sm-10 col-md-9">
                <h3>{{ $item->title }}</h3>
                <p>{{ substr($item->description,0,90) }}</p>
            </div>
            </a>
        </div>
        @endforeach

    </div>
</div>