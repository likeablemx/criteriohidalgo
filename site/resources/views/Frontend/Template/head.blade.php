<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title> @yield('title') </title>
        <meta name="description" content=" @yield('metadescription') ">

        <meta property="og:type" content="article" />
        <meta property="og:image" content=" @yield('mainimage')" />
	<meta property="og:description" content=" @yield('metadescription') ">
        <meta property="og:title" content="@yield('title')" />
        <meta property="og:site_name" content="criteriohidalgo.com" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="criteriohidalgo.com" />
        <meta name="twitter:title" content="@yield('title')" />
        <meta name="twitter:description" content="@yield('metadescription')" />
        <meta name="twitter:image:src" content="@yield('mainimage')" />

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/img/favicon.ico" rel="icon" type="image/x-icon" />

        <!--STYLES-->
        <link rel="stylesheet" media="print" href="{{ asset('css/plugins/bootstrap/bootstrap.min.css') }}">
        <link rel="stylesheet" media="print" href="{{ asset('css/plugins/bootstrap/bootstrap-datepicker.css') }}">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" media="print" href="/css/print.css">

	<!-- Richmedia -->
        <script src='http://www5.smartadserver.com/config.js?nwid=438' type="text/javascript"></script>
        <script type="text/javascript">
            sas.setup({ domain: 'http://www5.smartadserver.com'});
        </script>
	<style>
		#sas_20802{text-align:center}
	</style>


    </head>


    <body>

        <!-- Google Tag Manager -->
            <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KS8LW5"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KS8LW5');</script>
        <!-- End Google Tag Manager -->

	<!-- Edición body producción -->
<script type="text/javascript">
	sas.call("std", {
		siteId:		49809,	// 
		pageId:		342716,	// Página : criterio_de_hidalgo/home
		formatId: 	20802,	// Formato : DHTML-Rich_Media 1x1
		target:		''	// Segmentación
	});
</script>

<noscript>
	<a href="http://www5.smartadserver.com/ac?jump=1&nwid=438&siteid=49809&pgname=home&fmtid=20802&visit=m&tmstp=[timestamp]&out=nonrich" target="_blank">                
		<img src="http://www5.smartadserver.com/ac?out=nonrich&nwid=438&siteid=49809&pgname=home&fmtid=20802&visit=m&tmstp=[timestamp]" border="0" alt="" /></a>
</noscript>



	<!-- Intersticial -->

<script type='text/javascript'>
  (function() {
    var useSSL = 'https:' == document.location.protocol;
    var src = (useSSL ? 'https:' : 'http:') +
        '//www.googletagservices.com/tag/js/gpt.js';
    document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
  })();
</script>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    
googletag.defineOutOfPageSlot('/11322282/Criteriodehidalgo_INTERSTICIAL', 
'div-gpt-ad-1447274587227-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().enableSyncRendering();
    googletag.enableServices();
  });
</script>

<!-- /11322282/Criteriodehidalgo_INTERSTICIAL -->
<div id='div-gpt-ad-1447274587227-0'>
<script type='text/javascript'>

	<!-- Edición body producción -->
googletag.cmd.push(function() { 
googletag.display('div-gpt-ad-1447274587227-0'); });
</script>
</div>

	<!-- Intersticial -->
