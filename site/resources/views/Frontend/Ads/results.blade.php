@extends('master')
@section('content') 
<!-- buscador -->
{!!Form::Open(['url' => url('anuncios-clasificados/search'),'method' => 'post', 'class' => "form-horizontal"])!!}
    {!!Form::text('searchAds',null,['class'=>'form-control', 'placeholder' => 'Que buscas?'])!!}
    {!!$errors->first('searchAds','<div class="text-danger">:message</div>')!!}
{!!Form::Close()!!}
<!-- Resultados -->
<h2>Resultados de busqueda</h2>
<div class="renderMore">
    @if(count($ads) > 0)
        @foreach($ads as $item)
            <div class="itemAd">
                <p>Categoria: {{ $item->category->category->name }}</p>    
                <p>subCategoria: {{ $item->category->name }}</p>
                <p>Anuncio: <a href="/anuncios-clasificados/detalles/{{ $item->id }}"> {{ $item->title }}  </a>{{ $item->id }}</p>
                <hr>
            </div>
        @endforeach
    @else
        <p>No se encontraron resultados</p>
    @endif     
</div>

@include('Frontend.Template.footer')
    
@endsection
