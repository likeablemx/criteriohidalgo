<div id="moreCategories">   
    <h2 class="rightTitle">Más Categorías</h2>
    <section>
        <div class="customSelect">
           <!-- SUBCATEGORY AGREGARLE UN NUMERO DESPUES -> SUBCATEGORY1, SUBCATEGORY2 -->
            <label class="select" for="subcategory">Subcategoría</label>
            <div>
                <p class="customInput">Selecciona una subcategoría</p>
                <ul>
                    @foreach($categories as $item)
                    <li data-value="{{ $item['id'] }}" >{{ $item['name'] }}</li>
                    @endforeach
                </ul>
            </div>
            <!-- AGREGARLE UN NUMERO DESPUES EL ID -> SUBCATEGORY1, SUBCATEGORY2... -->
            <select id="subcategory" name="sub_category_id">
                @foreach($categories as $item)
                <option value="{{ $item['id'] }}" >{{ $item['name'] }}</option>
                @endforeach
            </select>
        </div>

    </section>
</div>