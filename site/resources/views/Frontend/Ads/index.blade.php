@extends('master')
@section('title'){{ $title }}@endsection
@section('content')  

<main>
    <div id="module_tittle_category" class="author_tittle">
        <div id="content">
            <h1>Anuncios clasificados</h1>
        </div>
    </div> 
    
    <div id="container">
        <div class="row">
            <div class="row">
                <!-- CONTENIDO PRINCIPAL HOME (IZQUIERDO)-->
                <div  class="col-xs-12 col-md-8" id="mainContent">
                    <div id="addsContent">
                        
                        <!-- Buscador -->        
                        <div id="addsSearch">
                            {!!Form::Open(['url' => url('anuncios-clasificados/search'),'method' => 'post', 'class' => "form-horizontal"])!!}
                              
                               <div class="input-group">
                                  {!!Form::text('search',null,['class'=>'form-control', 'placeholder' => ''])!!} 
                                   <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                  </span>
                               </div>
                               
                                {!!$errors->first('search','<div class="text-danger">:message</div>')!!}
                            {!!Form::Close()!!}
                        </div>

                        <!-- Categorias -->
                        <div id="addsCategory"> 
                            @foreach($categories as $element)
                               <section>
                                    <h3>{{ $element->name }}</h3>
                                    @foreach($element->sub_categories as $item)
                                        <p><a href="/anuncios-clasificados/categoria/{{ $item->slug }}">    {{ $item->name }}</a></p>
                                    @endforeach
                                </section>
                            @endforeach
                        </div>
                        
                        <!-- Recientes -->
                        <div id="addsLast" class="renderMore">
                            <h2>Lo último</h2>
                           
                            <!-- ANUNCIOS -->
                            @foreach($ads as $item)
                                <div class="itemAd">
                                    <a class="newsHover" href="/anuncios-clasificados/detalles/{{ $item->id }}">
                                    <section class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="cropImg">
                                                <img src="{{ asset("/images/ads/{$item->image}") }}" alt="Generica">
                                                <span class="category">{{ $item->category->category->name }}</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="info">
                                                <h3>{{ $item->title }}</h3>    
                                                <span>{{ $item->category->name }}</span>
                                                <p>{{ $item->description }}</p>
                                            </div>
                                        </div>
                                    </section>
                                    </a>
                                </div>
                            @endforeach
                               
                        </div>
                        
                        
                        <!-- Cargar mas -->
                        @if($count > 9)
                            <div id="loadMoreButtonContainer" class="module_notes_category">
                                <div id="btnMore">
                                    @if($slug != '')
                                        <a  onclick="moreAds('/anuncios-clasificados/categoria/{{$skip}}/','{{ $slug }}',{{ $count }});">CARGAR MÁS</a>
                                    @else
                                        <a  onclick="moreAds('/anuncios-clasificados/',{{ $skip }},{{ $count }});">CARGAR MÁS</a>
                                    @endif
                                </div>
                            </div>
                        @endif
      
                    </div>


                </div>
                <!-- CONTENIDO PRINCIPAL HOME (DERECHA)-->
                <div class="col-xs-12 col-md-3 col-md-offset-1 col-lg-4 col-lg-offset-0" id="rightContent">
                    @if(!$modulesR->modules->isEmpty())
                        @foreach($modulesR->modules as $key => $item)
                            @if($key == 1)
                                <div id="publishAd">
                                    <a href="/anuncios-clasificados/publicar"><img src="/img/boton-anuncio.jpg" alt="Anuncio" /></a>
                                </div>
                                <div id="moreCategories">
                                    <h2 class="rightTitle">Más Categorías</h2>
                                    <section>
                                        @foreach($categories as $element)
                                            <div class="customSelect">
                                                <label class="select" for="subcategory1">{{ $element->name }}</label>
                                                <div>
                                                    <p class="customInput">Selecciona una subcategoría</p>
                                                    <ul>
                                                        @foreach($element->sub_categories as $cat)
                                                            <li data-value="{{ $cat->id }}"><a href="/anuncios-clasificados/categoria/{{ $cat->slug }}">{{ $cat->name }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <select id="subcategory1" name="sub_category_id">
                                                    @foreach($element->sub_categories as $cat)
                                                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @endforeach
                                    </section>
                                </div>
                            @endif
                            @include("Frontend.Category.Modules.{$item->type_slug}")
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

    </div>


</main>


@include('Frontend.Template.footer')
    
@endsection
