@if(!empty($principal))
<div id="module_main_category">
   <a class="mainImageHover" href="{{ route('notes', \Frontend::getRoute($principal, $principal->slug)) }}">
        <div id="galleryContainer">
            <!-- IMAGEN PRINCIPAL  -->
            <div class="mainImage slide">
                <?php $gallery = $principal->galleries->first() ?>
                <div class="cropImg">
                    @if(!$gallery->elements->isEmpty())
                        <img alt="MainImage" src="{{ asset("{$gallery->directory}/{$gallery->elements[0]->value}") }}">
                    @else
                        <img src="/Backend/images/generica.jpg" alt="Generica">
                    @endif
                </div>
                @if($category->image)
                    <span class="category"><img src="{{ asset("/images/categories/{$category->image}") }}" alt="{{ $category->title }}"></span>
                @endif
                    <section>
                        <h2>{{ $principal->title }}</h2>
                        <p><span class="calendar"></span> {{ \Frontend::datePublishing($principal->date_publishing) }}</p>
                    </section>
            </div>
        </div>
    </a>
</div>
@endif