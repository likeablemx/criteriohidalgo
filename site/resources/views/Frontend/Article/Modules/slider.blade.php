<?php $gallery = $article->galleries->first() ?>
@if(!$gallery->elements->isEmpty())
    <?php unset($gallery->elements[0]) ?>
    @if(!$gallery->elements->isEmpty())
    <div id="module_slider">
        <div id="sliderGallery" class="royalSlider rsDefault">
            @foreach($gallery->elements as $item)
                @if($item->type == 'image')
                    <a class="rsImg bugaga" data-rsbigimg="/{{ $gallery->directory }}/{{ $item->value }}" href="/{{ $gallery->directory }}/{{ $item->value }}">
                        <img class="rsTmb" src="/{{ $gallery->directory }}/{{ $item->value }}">
                        <div class="infoBlock rsABlock" data-fade-effect="" data-move-offset="10" data-move-effect="bottom" data-speed="200">
                            <h4>{{ $item->title }}</h4>
                        </div>
                    </a>
                @endif
            @endforeach    
      </div>
    </div>
    @endif
@endif

