

<div id="module_main_note">
    <div id="galleryContainer">
        <!-- IMAGEN PRINCIPAL  -->
        <figure class="mainImage">
            @if(!$gallery->elements->isEmpty())
            <div class="cropImg">
                <img src="{{ asset("{$gallery->directory}/{$gallery->elements[0]->value}") }}">
            </div>
            <figcaption>{{ $gallery->elements[0]->title }}</figcaption>
            @endif
            <section>
                <h2>{{ $article->title }}</h2>
            </section>
        </figure>
    </div>
</div>
