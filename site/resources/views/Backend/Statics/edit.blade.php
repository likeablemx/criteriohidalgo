@extends('Backend.Template.layout')

@section('title') {{$title}} @stop

@section('content')
<div class="container">
    <div class="row">
	<div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
		<div class="panel-heading orange"><h4><span class="glyphicon glyphicon-list-alt"></span>{{ $item->name }}</h4></div>
		<div class="panel-body">
                    {!!Form::Open(['url' => route('statics.update',$item->id), 'files' => true, 'class' => "form-horizontal", 'role'=>'form'])!!}
                        @foreach($item->elements as $num => $element)                        
                            @if($num == 0)
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Título</label>
                                    <div class="col-md-8">
                                        {!! Form::text('title[]', $element->title,['class'=>'form-control']) !!}
                                        {!!$errors->first('title','<div class="text-danger">:message</div>')!!}
                                        {!! Form::hidden('text[]', $element->title,['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            @else
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Subtítulo</label>
                                    <div class="col-md-8">
                                        {!! Form::text('title[]', $element->title,['class'=>'form-control']) !!}
                                        {!!$errors->first('title','<div class="text-danger">:message</div>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Texto</label>
                                    <div class="col-md-8">
                                        {!! Form::textarea('text[]', $element->text,['class'=>'form-control text_rich', 'rows' => 5]) !!}
                                        {!!$errors->first('text','<div class="text-danger">:message</div>')!!}
                                    </div>
                                </div>

                                @if($element->image != null)
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <span class="label label-primary">Medidas de Imagen 364 x 476 (recomendado)</span>
                                                <img src="{{ asset("/images/statics_sections/{$item->slug}/{$element->image}") }}" class="img-thumbnail" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagen</label>
                                        <div class="col-md-8">
                                            {!! Form::file("image[]", ['class'=>'form-control']) !!}
                                            {!!$errors->first('image','<div class="text-danger">:message</div>')!!}
                                        </div>
                                    </div>
                                @endif
                            @endif
                            {!! Form::hidden('id[]', @$element->id) !!}
                            <hr />
                        @endforeach

			<div class="form-group text-center">
                            <div class="btn-group">
				<button type="submit" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-ok"></span>
                                    Actualizar
				</button>
                            </div>
			</div>
                    {!! Form::close() !!}
                    </div>
		</div>
	</div>
    </div>
</div>
@endsection
