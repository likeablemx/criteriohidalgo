<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="{{ asset('img/favicon.ico') }}" rel="icon" type="image/x-icon" />

	<title>@yield('title', 'mcPanel')</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	{{-- Cargador de archivos --}}
	<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
	<link rel="stylesheet" href="{{asset('Backend/vendor/jquery-file-upload/css/jquery.fileupload.css')}}">
	<link rel="stylesheet" href="{{asset('Backend/vendor/jquery-file-upload/css/jquery.fileupload-ui.css')}}">
	<noscript><link rel="stylesheet" href="{{asset('Backend/vendor/jquery-file-upload/css/jquery.fileupload-noscript.css')}}"></noscript>
	<noscript><link rel="stylesheet" href="{{asset('Backend/vendor/jquery-file-upload/css/jquery.fileupload-ui-noscript.css')}}"></noscript>
	{{--------------------------}}
	<link rel="stylesheet" type="text/css" href="{{ asset('Backend/vendor/tags/jquery.tagsinput.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('Backend/vendor/jquery-ui/jquery-ui.css') }}" />
	<!--/////***  Color picker  ***/////-->
	<link rel="stylesheet" type="text/css" href="{{ asset('Backend/vendor/colorPicker/css/bootstrap-colorpicker.min.css') }}" />

	<link rel="stylesheet" type="text/css" href="{{ asset('Backend/css/backend.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('Backend/css/style.css') }}" />



	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

    <!-- Menu -->
    @include('Backend.Template.menu')

	@include('Backend.Template.errors')

	@yield('content')

	<footer>
		<div class="container-fluid" align="center">
			<p>Criterio Hidalgo - Derechos Reservados 2015</p>
			<a href="/" role="button" aria-expanded="false" target="_blank"><span class="glyphicon glyphicon-globe"></span> Ir al sitio</a>
		</div>
	</footer>

	<!-- Scripts -->
	<script src="{{ asset('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') }}"></script>
	<script src="{{ asset('//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('Backend/vendor/bootstrap/bootstrap.min.js') }}"></script>
	<!-- TinyMCE -->
	<script src="{{ asset('Backend/vendor/tinymce/tinymce.min.js') }}"></script>
	<!-- /TinyMCE -->
	<!-- Tags -->
	<script src="{{ asset('Backend/vendor/tags/jquery.tagsinput.js') }}"></script>
	<!-- /Tags -->

	<!--/////***  Color picker  ***/////-->
	<script src="{{ asset('Backend/vendor/colorPicker/js/bootstrap-colorpicker.min.js') }}"></script>

	@if(\Request::route()->getName() == 'gallery')
	{{-- Scripts necesarios para la carga de archivos --}}
			<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
	<script src="{{ asset('Backend/vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
			<!-- The Templates plugin is included to render the upload/download listings -->
	<script src="{{ asset('//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js') }}"></script>
			<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="{{ asset('//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js') }}"></script>
			<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="{{ asset('//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js') }}"></script>

			<!-- blueimp Gallery script -->
	<script src="{{ asset('//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js') }}"></script>
			<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="{{ asset('Backend/vendor/jquery-file-upload/js/jquery.iframe-transport.js') }}"></script>
			<!-- The basic File Upload plugin -->
	<script src="{{ asset('Backend/vendor/jquery-file-upload/js/jquery.fileupload.js') }}"></script>
			<!-- The File Upload processing plugin -->
	<script src="{{ asset('Backend/vendor/jquery-file-upload/js/jquery.fileupload-process.js') }}"></script>
			<!-- The File Upload image preview & resize plugin -->
	<script src="{{ asset('Backend/vendor/jquery-file-upload/js/jquery.fileupload-image.js') }}"></script>
			<!-- The File Upload audio preview plugin -->
	<script src="{{ asset('Backend/vendor/jquery-file-upload/js/jquery.fileupload-audio.js') }}"></script>
			<!-- The File Upload video preview plugin -->
	<script src="{{ asset('Backend/vendor/jquery-file-upload/js/jquery.fileupload-video.js') }}"></script>
			<!-- The File Upload validation plugin -->
	<script src="{{ asset('Backend/vendor/jquery-file-upload/js/jquery.fileupload-validate.js') }}"></script>
			<!-- The File Upload user interface plugin -->
	<script src="{{ asset('Backend/vendor/jquery-file-upload/js/jquery.fileupload-ui.js') }}"></script>
	@endif

	<!-- Funciones -->
	<script src="{{ asset('Backend/js/functions.js') }}"></script>
    <script src="{{ asset('Backend/js/MainSlider.js') }}"></script>

</body>
</html>
