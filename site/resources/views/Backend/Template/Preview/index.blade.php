@extends('Backend.Template.Preview.layout')
@section('title'){{ $article->title }}@endsection
@section('metadescription'){{ $article->description }}@endsection
@section('content')

<main>
    <?php $gallery = $article->galleries->first() ?>
    @include('Frontend.Category.Modules.titulo', ['category' => $article->category])
  <audio id="audioPlayer" autoplay="autoplay"></audio>
    <div id="container">
        <div class="row">
           
            <!-- CONTENIDO PRINCIPAL HOME (IZQUIERDO)-->
            <div  class="col-xs-12 col-md-8" id="mainContent">  

                @include('Frontend.Article.Modules.main')
                
                <div id="noteContainer">
                    <div>
                        <div id="lectureControl">
                            <a onclick="playLector();">Comenzar Lectura</a>
                            <a onclick="stopLector();">Detener</a>  
                        </div>
                        
                        <div >
                            <p id="resume">{!! $article->intro !!}</p>
                            {!! \Frontend::replaceShortCuts($article->content, 'video') !!}
                        </div>
                        <?php $text = strip_tags($article->intro.$article->content) ?>
                        <div id="lectorApi" class="hidden"> {!! $text !!} </div>
                        
                        @include('Frontend.Article.Modules.slider')
                        @include('Frontend.Article.Modules.issu')
                        @include('Frontend.Article.Modules.disquss')
                    </div>
                    <aside>
                        @include('Frontend.Article.Modules.author')
                        @include('Frontend.Article.Modules.social')
                        @include('Frontend.Article.Modules.tags')
                        @include('Frontend.Article.Modules.related')
                    </aside>
                </div>

            </div>

            <!-- CONTENIDO PRINCIPAL HOME (DERECHA)-->
            <div class="col-xs-12 col-md-3 col-md-offset-1 col-lg-4 col-lg-offset-0" id="rightContent">
                @if(!$article->category->modules->isEmpty())
                    @foreach($article->category->modules as $item)
                        @include("Frontend.Category.Modules.{$item->type_slug}")
                    @endforeach
                @endif
            </div>
        </div>
    </div>

</main>

@endsection