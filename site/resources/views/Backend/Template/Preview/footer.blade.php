    <footer>
        <div id="footerMenu">
            <ul>
                <li><a href="/noticias/hidalgo">Noticias</a></li>
                <li><a href="/regiones/tulancingo">Regiones</a></li>
                <li><a href="/sos">Sos</a></li>
                <li><a href="/a-criterio/cartones">A criterio</a></li>
                <li><a href="/suplementos/bon-appetit">Suplementos</a></li>
                <li><a href="/especiales">Especiales</a></li>
                <li><a href="/ticket/curiosidades">Ticket</a></li>
                <li><a href="/la-copa/nfl">La Copa</a></li>
                <li><a href="/multimedia/videos">Multimedia</a></li>
                <li><a href="/first-class">First Class</a></li>
                <li><a href="/politics">Politics</a></li>
                <li id="goTop"><a href="#">Volver Arriba <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></li>
            </ul>
        </div>
        
        <div id="bottom">
           <div class="row">
               <div id="footerLogo" class="col-xs-12 col-sm-6 col-md-3">
                    <img src="{{ asset('/img/logo.png') }}" alt="Logo" />   
               </div>
               
               <div id="footerText" class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <p>Criterio, la verdad impresa, nació para informarte; este es un sitio pensado para ti: úsalo, retroaliméntalo, siempre con respeto a las ideas de los demás.</p>       
               </div>
               
               <div id="links" class="col-xs-12 col-md-3 col-lg-3">
                   <ul>
                       <li><a href="{{ url('nosotros') }}">Nosotros</a></li>
                       <li><a href="{{ url('contacto') }}">Contacto</a></li>
                       <li><a href="{{ url('directorio') }}">Directorio</a></li>
                       <li><a href="{{ url('aviso-privacidad') }}">Aviso de privacidad</a></li>
                   </ul>
               </div>
           </div>
            
            
        </div>
    </footer>
    
    <!-- MAIN -->
    <script src="{{ asset('/js/jquery.js') }}"></script>
