<div class="col-md-10 col-md-offset-1">
    <div class="panel panel-heading buttons">

        {!! Form::open(['route' => 'filter', 'method' => 'post', 'class'=>'form-inline']) !!}

            <div class="form-group-block searchButtons">
                <div>
                    <h2>Buscador</h2>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span> Buscar</button>
                    <!--{!! Form::button('Buscar', ['type' => 'submit', 'class' => 'btn btn-default']) !!}-->
                </div>
                <div class="form-group">
                    <a href="{{ url('mcPanel/limpiar') }}" class="btn btn-default"><span class="glyphicon glyphicon-refresh"></span> Limpiar</a>
                </div>
            </div>

            <div id="searchParameters">
                <div class="form-group">
                    {!! Form::text('filter[key]', @Session::get('filters.key'), ['placeholder' => 'Palabra clave', 'class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::select('filter[field]', $search['fields'], @Session::get('filters.field'), ['class' => 'form-control']) !!}
                </div>

                @if(!is_null($search['filters']))
                    @foreach($search['filters'] as $filter => $value)
                        <div class="form-group">
                            {!! Form::select("filter[{$filter}]", $value, @Session::get("filters.{$filter}"), ['class' => 'form-control']) !!}
                        </div>
                    @endforeach
                @endif

                <div class="form-group">
                    {!! Form::select('filter[order]', ['ASC' => 'Ascendente', 'DESC' => 'Descendente'], @Session::get('filters.order'), ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::select('filter[show]', Util::range(10, 100, 10), @Session::get('filters.show'), ['class' => 'form-control']) !!}
                </div>
            </div>

            {!! Form::hidden('filter[form]', $form) !!}

        {!! Form::close() !!}
    </div>
</div>
    