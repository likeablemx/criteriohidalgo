@extends('Backend.Template.layout')

@section('content')

<div class="container-fluid">
	<div class="row" id="dashboard">
		<div class="col-md-8 col-md-offset-2">
            <h1 id="mainTitle">Dashboard</h1>

            <div class="panel panel-default">
  				<div class="panel-body">
  					<h2>¡Bienvenido(a) {{ Auth::user()->name }}!</h2>
    				<p>Dentro del gestor de contenidos de <strong>Criterio Hidalgo</strong>, podrá editar toda la información del sitio.</p>

    				<hr />

    				<p>Para acceder rápidamente a editar cualquier elemento del sitio, favor de elegir entre los
                        siguientes botones o puede navegar desde el menú principal.</p>

                    <hr />

					@if(\Auth::user()->role->type <> 'Editor')
					<div class="col-md-2">
						<a href="{{ route('users') }}" class="btn btn btn-lg">
							<span class="glyphicon glyphicon-user"></span> Usuarios
						</a>
					</div>
					@endif

					<div class="col-md-2">
						<a href="{{ route('autores') }}" class="btn btn btn-lg">
							<span class="glyphicon glyphicon-tasks"></span> Autores
						</a>
					</div>
					@if(\Auth::user()->role->type <> 'Editor')
					<div class="col-md-2">
						<a href="{{ route('categories') }}" class="btn btn-lg">
							<span class="glyphicon glyphicon-list"></span> Categorías
						</a>
					</div>
					@endif
					<div class="col-md-2">
						<a href="{{ route('articles') }}" class="btn btn-lg">
							<span class="glyphicon glyphicon-file"></span> Notas
						</a>
					</div>
  				</div>
			</div>
        </div>
	</div>
</div>
@endsection