@extends('Backend.Template.layout')

@section('title') Edición de galerías @stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <ul class="nav nav-tabs nav-tabs-form">
                    <li><a href="{{ url("mcPanel/notas/registro/{$gallery->imageable->id}") }}"><h4><span class="glyphicon glyphicon-list-alt"></span> Articles</h4></a></li>
                    <li class="active"><a href="#"><h4><span class="glyphicon glyphicon-picture"></span> Galería</h4></a></li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p style="margin-top: 20px;"><strong>Importante: La primera imagen de tu galería es la que muestra el pie de imagen en el sitio web. Las demás imágenes tienen el campo de texto habilitado en caso de que desees cambiar tu imagen principal por alguna de las otras imágenes.</strong></p>
                        <p style="margin-top: 20px;">Para cambiar tu imagen principal sólo arrastra la imagen que desees cambiar a la primera posición (imagen esquina superior izquierda)</p>
                        {!!Form::open(['route'=>'gallery.elemento.eliminar.multi','class'=>'formMultiSelect'])!!}
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger tobe_eliminated_submit"><span class="glyphicon glyphicon-remove-sign"></span> Eliminar</button>
                        </div>

                        <div class="form-group">
                            <input type="checkbox" id="multiSelect">
                            <label for="multiSelect">Seleccionar todos</label>
                        </div>
                        {!!Form::close()!!}

                        <hr>

                        <div class="container-fluid" id="gallery-container">
                            <div class="row">
                                @if(count($gallery->elements))
                                    <ul id="sortable" data-content="galeria">
                                    @foreach($gallery->elements as $element)
                                        <li id="{{ $element->id }}" class="col-sm-6 col-md-3">@include('Backend.Galleries.element')</li>
                                    @endforeach
                                    </ul>
                                @else
                                    <h4>No hay elementos</h4>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
                <div id="msj_error" class="alert alert-danger alert-dismissible" style="display: none" role="alert">
                    <strong>Error!!!</strong> La imágen <strong id="img_error_exd"></strong> rebasa el limite de peso permitido.
                </div>
                @include('Backend.Galleries.loader')
            </div>
        </div>
    </div>

@stop