@extends('Backend.Template.layout')

@section('title') {{$title}} @stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! $message !!}
                <div class="panel panel-default">
                    <div class="panel-heading orange"><h4><span class="glyphicon glyphicon-list-alt"></span> {{ $title }} </h4></div>
                    <div class="panel-body">

                        {!!Form::Open(['url' => route('directorio.update',$id), 'files' => true, 'class' => "form-horizontal", 'role'=>'form'])!!}
                            <div class="form-group">
                                {!!Form::Label('name','Nombre:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('name',@$item->name,['class'=>'form-control'])!!}
                                    {!!$errors->first('name','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('title','Titulo:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('title',@$item->title,['class'=>'form-control'])!!}
                                    {!!$errors->first('title','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('mail','Correo:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('mail',@$item->mail,['class'=>'form-control'])!!}
                                    {!!$errors->first('mail','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                    <a href="{{ route('directorio') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Regresar</a>
                                </div>
                            </div>
                        {!!Form::Close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop