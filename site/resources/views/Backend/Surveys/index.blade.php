@extends('Backend.Template.layout')

@section('title') Encuestas @stop

@section('content')
    <div class="container panel panel-default">
        <h2>Lista de Encuestas</h2>
        <p style="margin-top: 20px;">1) Haz click en el botón para crear una nueva encuesta.</p>
        <p>Puedes crear las encuestas que quieres, pero solo una será la que se muestre en la página principal del sitio.</p>
        <p style="margin-bottom: 20px;">Para seleccionar la encuesta que quieres que sea la que se muestre, debes irte al menú de abajo en acciones y dar click editar (icono de lápiz), despúes seleccionar la opción final que dice "Esta es la encuesta actual" y guardar los cambios.</p>
        <div class="form-group">
            <a class="btn btn-primary" href="{{route('surveys.edit')}}">
                <span class="glyphicon glyphicon-plus"></span> Nuevo
                
            </a>
        </div>

        <hr />
            {!! $filters !!}
        <hr/>

        {!! $message !!}

        @if(!$list->isEmpty())
            {!!Form::Open(['class'=>'form','id'=>'form-multi-select'])!!}
                <div class="text-left form-group">
                    <label class="control-label">Elementos seleccionados </label>
                    <a data-href="{{route('surveys.delete')}}" data-action="ELIMINAR" class="btn btn-danger task-multi-select" id="delete"><i class="glyphicon glyphicon-remove-sign"></i> Eliminar</a>
                </div>
            {!!Form::close()!!}

            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th><input type="checkbox" class="multi-select-all"> </th>
                                <th>Pregunta</th>
                                <th>Creada</th>
                                <th>Activa</th>
                                <th colspan="2">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list as $item)
                                <tr class="allRow" data-item="{{ $item->id }}">
                                    <td><input type="checkbox" class="select-checkbox" name="selected[]" value="{{$item->id}}" /></td>
                                    <td>{{$item->sentence}}</td>
                                    <td>{{$item->created_at}}</td>
                                    <td>{{($item->current) ? 'SI' : 'NO' }}</td>
                                    <td><a class="btn btn-primary" href="{{route('surveys.edit',$item->id)}}"><span class="glyphicon glyphicon-edit"></span> Editar</a></td>
                                    {{--<td><a href="{{route('surveys.delete',$item->id)}}"><span class="glyphicon glyphicon-remove"></span> </a> </td>--}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {!!$list->render()!!}
        @else
            <h2>No hay resultados</h2>
        @endif
    </div>

@stop