@extends('Backend.Template.layout')

@section('title') {{$title}} @stop

@section('content')
    <div class="container">
        <div class="reporte" style="background-color: #CEDAFF; padding-left: 30px; border-radius: 15px;">
            @if(!empty($item->options))
                @include('Backend.Surveys.results',['item'=>$item])
                
            @endif
        </div>
        <h2> Encuesta </h2>
        <p>1) Crea tu encuesta escribiendo una pregunta y dando opción de hasta 5 respuestas.</p>

        {!!Form::Open(['url'=>route('surveys.update',$id),'class'=>"form-horizontal",'role'=>'form'])!!}

            <div class="form-group">               
                {!!Form::Label('sentence','Pregunta :',['class'=>'control-label col-sm-2'])!!}
                <div class="col-sm-10">
                    {!!Form::textArea('sentence',@$item->sentence,['class'=>'form-control','rows'=>2])!!}
                    {!!$errors->first('sentence','<div class="text-danger">:message</div>')!!}
                </div>
            </div>

            <h3>Opciones</h3>
            {!!$errors->first('options','<div class="text-danger">:message</div>')!!}

            @for($i=0;$i<5;$i++)
                <?php $option = isset($item->options[$i]) ? $item->options[$i] : null; ?>
                @include('Backend.Surveys.option',['i'=>$i,'option'=>$option])
            @endfor

            <div class="form-group">
                <div class="checkbox col-sm-offset-5">
                    <label>{!!Form::checkbox('current','1',!empty($item->current))!!} Esta es la encuesta actual</label>
                    
                </div>
            </div>
            <div class="form-group">
                <div class="text-center">
                    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                    <a href="{{ route('surveys') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Regresar</a>
                </div>
            </div>

        {!!Form::Close()!!}
    </div>
@stop