<div class="container-fluid">
    {!!Form::open(['url'=>route('surveys.filter'),'class'=>'form form-vertical','role'=>'form'])!!}
        <div class="form-group col-sm-4">
            {!!Form::label('keyword','Buscar')!!}
            {!!Form::text('keyword',$filters['keyword'],['placeholder'=>'Pregunta','class'=>'form-control'])!!}
        </div>
        <div class="form-group col-sm-4">
            {!!Form::label('items','Mostrar')!!}
            {!!Form::select('items',['5'=>'5','10'=>'10','20'=>'20'],$filters['items'],['class'=>'form-control'])!!}
        </div>
        <div class="text-left form-group btn-group col-sm-12">
            <!-- {!!Form::submit('Filtrar',['class'=>'btn btn-success'])!!} -->
             <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Filtrar</button>
            <a class="btn btn-warning" href="{{route('surveys.filter')}}"><i class="glyphicon glyphicon-refresh"></i> Limpiar Filtros</a>
        </div>
    {!!Form::close()!!}
</div>