@extends('Backend.Template.layout')

@section('title') Mensajes: detalles @stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading orange"><h4><span class="glyphicon glyphicon-list-alt"></span> Mensajes: detalles </h4></div>
                    <div class="panel-body">

                        {!!Form::Open(['url' => route('mensajes.update',$id), 'files' => true, 'class' => "form-horizontal", 'role'=>'form'])!!}
                            <div class="form-group">
                                <div class="col-md-8">
                                    @foreach(json_decode($item->values) as $field => $value)
                                        {{htmlentities($field)}}: {{htmlentities($value)}} <br />
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <div class="btn-group">
                                    <a href="{{ route('mensajes') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Regresar</a>
                                </div>
                            </div>
                        {!!Form::Close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop