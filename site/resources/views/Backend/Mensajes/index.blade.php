@extends('Backend.Template.layout')

@section('title') Mensajes 
    @if($list->sum('seen') > 0)
        ({{ $list->sum('seen') }}) 
    @endif
@stop

@section('content')

    <div class="container">
        <h1>Mensajes</h1>
        <a href="{{ route('forms') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Regresar</a>
        <a class="btn btn-primary" href="{{route('forms.reporte',1)}}">
                    <span class="glyphicon glyphicon-download-alt"></span> Descargar Reporte
                </a>
        <hr />

        {!! $filters !!}

        <hr/>

        {!! $message !!}

        @if(!$list->isEmpty())
            Total: {{$list->total()}}
            {!!Form::Open(['class'=>'form','id'=>'form-multi-select'])!!}
                <div class="text-left form-group">
                    <label class="control-label">Elementos Seleccionados: </label>
                    <a data-href="{{route('mensajes.delete')}}" data-action="ELIMINAR" class="btn btn-danger task-multi-select" id="delete"><i class="glyphicon glyphicon-remove-sign"></i> Eliminar</a>
                </div>
            {!!Form::close()!!}

            <div class="panel panel-default">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><input type="checkbox" class="multi-select-all"> </th>
                        <th>Fecha</th>
                        <th>Correo Electr&oacute;nico</th>
                        <th>Visto</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($list as $item)
                        <tr>
                            <td><input type="checkbox" class="select-checkbox" name="selected[]" value="{{$item->id}}" /></td>
                            <td>{{htmlentities($item->created_at)}}</td>
                            <td>{{htmlentities($form->getMainEmailValue($item->id))}}</td>
                            <td>{{($item->seen) ? 'NO' : 'SI' }}</td>
                            <td><a class="btn btn-primary" href="{{route('mensajes.details',$item->id)}}"><span class="glyphicon glyphicon-edit"></span> Detalles</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
                </div>
            </div>
            {!!$list->render()!!}
        @else
            <h2>No hay resultados</h2>
        @endif
    </div>

@stop