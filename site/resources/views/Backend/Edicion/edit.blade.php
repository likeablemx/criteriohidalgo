@extends('Backend.Template.layout')

@section('title') {{$title}} @stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! $message !!}
                <div class="panel panel-default">
                    <div class="panel-heading orange"><h4><span class="glyphicon glyphicon-list-alt"></span> {{ $title }} </h4></div>
                    <div class="panel-body">

                        {!!Form::Open(['url' => route('edicion.update',$id), 'files' => true, 'class' => "form-horizontal", 'role'=>'form'])!!}
                            <div class="form-group">
                                {!!Form::Label('name','Titulo:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('title',@$item->title,['class'=>'form-control'])!!}
                                    {!!$errors->first('title','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::Label('title','Fecha de publicacion:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                    {!!Form::text('date_publishing',@$item->date_publishing,['class'=>'form-control datepicker'])!!}
                                    {!!$errors->first('date_publishing','<div class="text-danger">:message</div>')!!}
                                </div>
                            </div>
                            <div class="form-group">
				{!!Form::Label('photo','Imagen:',['class'=>'col-md-3 control-label'])!!}
				<div class="col-md-8">
				@if(!empty($item->image))
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                                <span class="label label-primary">Medidas de Imagen 400 x 520 (recomendado)</span>
                                                <img src="{{ asset("/images/editions/{$item->image}") }}" class="img-thumbnail" />
                                        </div>
                                    </div>
				@endif
				{!! Form::file('image', null,['class'=>'form-control']) !!}
                                {!!$errors->first('image','<div class="text-danger">:message</div>')!!}
				</div>
                            </div>
                             <div class="form-group">
                                {!!Form::Label('active', 'Visible:',['class'=>'col-md-3 control-label'])!!}
                                <div class="col-md-8">
                                {!!Form::checkbox('active', '1', !empty($item->active))!!}
                                </div>
                             </div>
                            <div class="form-group text-center">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                    <a href="{{ route('edicion') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Regresar</a>
                                </div>
                            </div>
                        {!!Form::Close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop