@extends('Backend.Template.layout')

@section('title') {{$title}} @stop

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                {!! $message !!}

                <ul class="nav nav-tabs nav-tabs-form">
                    <li class="active"><a href="#"><h4><span class="glyphicon glyphicon-list-alt"></span> {{ $title }}</h4></a></li>
                    @if(@$item->exists)
                    <li><a href="{{ route('gallery', ['id' => $item->galleries->first(), 'type' => 'blog']) }}"><h4><span class="glyphicon glyphicon-picture"></span> Galería</h4></a></li>
                    <li><a href="{{ route('articles.preview', ['id' => $item->id]) }}" target="_blank"><h4><span class="glyphicon glyphicon-search"></span> Vista previa</h4></a></li>
                    @endif
                </ul>

                <div class="panel panel-default panel-form">
                    <div class="panel-body">
                        {!!Form::Open(['url' => route('articles.update', $id), 'files' => true, 'class' => "form-horizontal", 'role'=>'form'])!!}
                        <div class="form-group">
                            {!!Form::Label('title', 'Título:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                {!!Form::text('title', @$item->title,['class'=>'form-control'])!!}
                                {!!$errors->first('title','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('category_id', 'Categoría:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                @include('Backend.Template.category', ['list' => $categories, 'value' => @$item->category_id])
                                {!!$errors->first('category_id','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('author_id', 'Autores:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                <div class="wrapperInputLoading">
                                {!!Form::text('autocomplete-name', @$item->authors->name,['class'=>'form-control','id' =>'autocomplete-name', 'autocomplete' => 'off'])!!}
                                {!!Form::hidden('author_id', @$item->author_id,['class'=>'form-control'])!!}
                                <div class="preloader-wrapper smaller">
                                    <div class="spinner-layer spinner-red-only">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="gap-patch">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                {!!$errors->first('author_id','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('issuu', 'Issuu:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                <span class="label label-primary">Copia el código de un documento ISSUU a través de la opción “Embed” y pégalo en el campo siguiente.</span>
                                {!!Form::textarea('issuu', @$item->issuu,['class'=>'form-control', 'rows' => 2])!!}
                                {!!$errors->first('issuu','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('publish', 'Periodo:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-4 text-right">
                                {!!Form::Label('publish', 'Fecha de publicación:',['class'=>'col-md-6 control-label'])!!}
                                <div class="col-md-6 input-group">
                                {!!Form::text('date_publishing', @$item->date_publishing,['class'=>'form-control datepicker', 'readonly'])!!}
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                {!!$errors->first('date_publishing','<div class="text-danger">:message</div>')!!}
                            </div>
                            <div class="col-md-4 text-right">
                                {!!Form::Label('publish', 'Fecha de expiración:',['class'=>'col-md-6 control-label'])!!}
                                <div class="col-md-6 input-group">
                                    {!!Form::text('date_expire', @$item->date_expire,['class'=>'form-control datepicker', 'readonly'])!!}
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                {!!$errors->first('date_expire','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('intro', 'Introducción:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                <span class="label label-primary">Se recomienda un máximo de 220 caracteres.</span>
                                {!!Form::textarea('intro', @$item->intro,['class'=>'form-control', 'rows' => 2, 'maxlength' => 220])!!}
                                {!!$errors->first('intro','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('content', 'Contenido:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                <span style="white-space:normal;" class="label label-primary">Galería de videos: insertar las etiquetas "youtube" junto con las claves de videos separados por comas, Ej. {{ "{youtube}IdYou1,IdYou2{/youtube}" }}. <br>NOTA: Solo se puede insertar una galería de videos</span>
                                {!!Form::textarea('content', @$item->content,['class'=>'form-control text_rich', 'rows' => 5])!!}
                                {!!$errors->first('content','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('publish', 'Comportamiento:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                {!!Form::Label('active', 'Visible:',['class'=>'control-label'])!!}
                                {!!Form::checkbox('active', '1', !empty($item->active))!!}

                                {!!Form::Label('featuring', 'Destacado:',['class'=>'control-label'])!!}
                                {!!Form::checkbox('featuring', '1', !empty($item->featuring))!!}

                                {!!Form::Label('principal', 'Principal:',['class'=>'control-label'])!!}
                                {!!Form::checkbox('principal', '1', !empty($item->principal))!!}

                                {!!Form::Label('principal', 'Autor:',['class'=>'control-label'])!!}
                                {!!Form::checkbox('author', '1', !empty($item->author))!!}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            {!!Form::Label('ids', 'Ids:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-sm-8">
                                <span class="label label-primary">Separa por comas los id's de las notas a relacionar.</span>
                                {!!Form::text('ids', @$item->ids,['class'=>'form-control tags'])!!}
                                {!!$errors->first('ids','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('tags', 'Tags:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-sm-8">
                                <span class="label label-primary">Separa por comas los tags a relacionar.</span>
                                {!!Form::text('tags', @$item->tags,['class'=>'form-control tags'])!!}
                                {!!$errors->first('tags','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::Label('description', 'Meta descripción:',['class'=>'col-md-3 control-label'])!!}
                            <div class="col-md-8">
                                <span class="label label-primary">Se recomienda un máximo de 150 caracteres.</span>
                                {!!Form::textarea('description', @$item->description, ['class' => 'form-control', 'rows' => 2, 'maxlength' => 150])!!}
                                {!!$errors->first('description','<div class="text-danger">:message</div>')!!}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group text-center">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
                                <a href="{{ route('articles') }}" class="btn btn-default"><i class="glyphicon glyphicon-arrow-left"></i> Regresar</a>
                            </div>
                        </div>
                        {!!Form::Close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

