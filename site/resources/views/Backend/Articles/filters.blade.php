<div class="container-fluid">
    {!!Form::open(['url' => route('articles.filter'),'class'=>'form form-vertical','role'=>'form'])!!}
        <div class="form-group col-sm-3">
            {!!Form::label('keyword','Buscar')!!}
            {!!Form::text('keyword', $filters['keyword'], ['placeholder'=>'Título de la nota','class'=>'form-control'])!!}
        </div>
        <div class="form-group col-sm-3">
            {!!Form::label('category_id', 'Categoría')!!}
            @include('Backend.Template.category', ['list' =>  $extra['categories'], 'value' => $filters['category_id'], 'assigned' => $extra['assigned']])
        </div>
        <div class="form-group col-sm-3">
            {!!Form::label('author_id', 'Autores')!!}
            <?php $parent = ['' => 'Seleccionar'] + $extra['authors'] ?>
            {!!Form::select('author_id', $parent, $filters['author_id'],['class'=>'form-control'])!!}
        </div>
        <div class="form-group col-sm-2">
            {!!Form::label('active','Estatus')!!}
            {!!Form::select('active', ['' => 'Seleccionar', 'si' => 'Habilitado', 'no' => 'Bloqueado'], $filters['active'],['class'=>'form-control'])!!}
        </div>
        <div class="form-group col-sm-1">
            {!!Form::label('items','Mostrar')!!}
            {!!Form::select('items',['5'=>'5','10'=>'10','20'=>'20','50'=>'50'], $filters['items'], ['class'=>'form-control'])!!}
        </div>
        <div class="checkbox col-sm-12">
            <label>{!!Form::checkbox('featuring','1', @$filters['featuring'])!!} Destacados</label>
            <label>{!!Form::checkbox('principal','1', @$filters['principal'])!!} Principales</label>
            <label>{!!Form::checkbox('inverse','1', @$filters['inverse'])!!} Orden Inverso</label>
        </div>
        <div class="text-left form-group btn-group col-sm-12">

            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Filtrar</button>
            <a class="btn btn-warning" href="{{route('articles.filter')}}"><i class="glyphicon glyphicon-refresh"></i> Limpiar Filtros</a>
        </div>
    {!!Form::close()!!}
</div>