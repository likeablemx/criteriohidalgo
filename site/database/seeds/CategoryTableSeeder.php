<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'id'        => 1,
                'lft'       => 1,
                'rgt'       => 2,
                'depth'     => 0,
                'title'     => 'A Criterio',
                'slug'      => 'a-criterio',
                'position'  => 4,
            ],
            [
                'id'        => 2,
                'lft'       => 3,
                'rgt'       => 4,
                'depth'     => 0,
                'title'     => 'Especiales',
                'slug'      => 'especiales',
                'position'  => 6,
            ],
            [
                'id'        => 3,
                'lft'       => 5,
                'rgt'       => 6,
                'depth'     => 0,
                'title'     => 'First Class',
                'slug'      => 'first-class',
                'position'  => 10,
            ],
            [
                'id'        => 4,
                'lft'       => 7,
                'rgt'       => 8,
                'depth'     => 0,
                'title'     => 'La Copa',
                'slug'      => 'la-copa',
                'position'  => 8,
            ],
            [
                'id'        => 5,
                'lft'       => 9,
                'rgt'       => 10,
                'depth'     => 0,
                'title'     => 'Multimedia',
                'slug'      => 'multimedia',
                'position'  => 9,
            ],
            [
                'id'        => 6,
                'lft'       => 11,
                'rgt'       => 12,
                'depth'     => 0,
                'title'     => 'Noticias',
                'slug'      => 'noticias',
                'position'  => 1,
            ],
            [
                'id'        => 7,
                'lft'       => 13,
                'rgt'       => 14,
                'depth'     => 0,
                'title'     => 'Politics',
                'slug'      => 'politics',
                'position'  => 11,
            ],
            [
                'id'        => 8,
                'lft'       => 13,
                'rgt'       => 14,
                'depth'     => 0,
                'title'     => 'Regiones',
                'slug'      => 'regiones',
                'position'  => 2,
            ],
            [
                'id'        => 9,
                'lft'       => 15,
                'rgt'       => 16,
                'depth'     => 0,
                'title'     => 'SOS',
                'slug'      => 'sos',
                'position'  => 3,
            ],
            [
                'id'        => 10,
                'lft'       => 17,
                'rgt'       => 18,
                'depth'     => 0,
                'title'     => 'Suplementos',
                'slug'      => 'suplementos',
                'position'  => 5,
            ],
            [
                'id'        => 11,
                'lft'       => 19,
                'rgt'       => 20,
                'depth'     => 0,
                'title'     => 'Ticket',
                'slug'      => 'ticket',
                'position'  => 7,
            ]
        ]);
    }
}
