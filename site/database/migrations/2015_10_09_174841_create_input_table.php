<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inputs',function(Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->integer('module_id')
                    ->unsigned();
            $table->string('type');
            $table->string('name');
            $table->text('options');
            $table->integer('position')
                    ->unsigned();
            $table->boolean('required');
            $table->timestamps();

            $table->foreign('module_id')
                    ->references('id')
                    ->on('modules')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
