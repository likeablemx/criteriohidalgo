<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Tabla de tags
        Schema::Create('ids',function(Blueprint $table)
        {
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->string('value');
        });

        Schema::create('idssables',function(Blueprint $table)
        {
            $table->engine = 'innoDB';

            $table->integer('id_id')->unsigned();
            $table->integer('idssable_id')->unsigned();
            $table->string('idssable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::Drop('ids');
        Schema::Drop('idssable');
    }
}
