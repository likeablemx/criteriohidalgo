<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Tabla de tags
        Schema::Create('tags',function(Blueprint $table)
        {
            $table->engine = 'innoDB';

            $table->increments('id');
            $table->string('value');
            $table->string('slug');
        });

        Schema::create('taggables',function(Blueprint $table)
        {
            $table->engine = 'innoDB';

            $table->integer('tag_id')->unsigned();
            $table->integer('taggable_id')->unsigned();
            $table->string('taggable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::Drop('tags');
        Schema::Drop('taggables');
    }
}
