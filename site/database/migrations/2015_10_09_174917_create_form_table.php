<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms',function(Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->text('to');
            $table->text('response');
            $table->string('subject');
            $table->string('submit');
            $table->string('thanks');
            $table->timestamps();
        });


        Schema::table('inputs',function(Blueprint $table) {
            $table->engine = 'InnoDB';
            
            //Input::truncate();
            $table->dropForeign('inputs_module_id_foreign');
            $table->dropColumn('module_id');

            $table->integer('form_id')
                    ->after('id')
                    ->unsigned();

            $table->string('slug')
                    ->after('name');

            $table->foreign('form_id')
                ->references('id')
                ->on('forms')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('inputs',function(Blueprint $table) {
            Input::truncate();
            $table->dropForeign('inputs_form_id_foreign');
            $table->dropColumn('form_id');
            $table->dropColumn('slug');
            $table->integer('module_id')
                    ->unsigned();

            $table->foreign('module_id')
                ->references('id')
                ->on('modules')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::drop('forms');
    }
}
